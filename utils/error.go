package utils

import (
	"net/http"

	"github.com/gin-gonic/gin"
	grpcErrors "github.com/thegeorgenikhil/course-platform/pkg/grpc_errors"
	"google.golang.org/grpc/status"
)

type errorResponse struct {
	Code    int    `json:"code"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

func HandleError(ctx *gin.Context, status int, msg string) {
	ctx.JSON(status, &errorResponse{
		Code:    status,
		Error:   http.StatusText(status),
		Message: msg,
	})
}

func HandleGrpcError(ctx *gin.Context, err error) {
	s := status.Convert(err)
	code := grpcErrors.GetHttpStatusCodeFromGrpcErrorCode(s.Code())
	msg := s.Message()
	ctx.JSON(
		code,
		&errorResponse{
			Code:    code,
			Error:   http.StatusText(code),
			Message: msg,
		},
	)
}
