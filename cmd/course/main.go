package main

import (
	"context"
	"log"

	"github.com/thegeorgenikhil/course-platform/cmd/course/config"
	"github.com/thegeorgenikhil/course-platform/internal/course/app"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatalln("not able to load config: ", err)
	}

	a := app.New(cfg)

	if err = a.Run(context.Background()); err != nil {
		log.Fatalln("app run failed: ", err)
	}
}
