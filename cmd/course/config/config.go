package config

import (
	"fmt"
	"log"
	"os"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/thegeorgenikhil/course-platform/pkg/config"
)

type Config struct {
	config.App    `yaml:"app"`
	config.GRPC   `yaml:"grpc"`
	config.Log    `yaml:"logger"`

	MongoDB struct {
		ConnStr string `env-required:"true" yaml:"connection_uri" env:"DB_CONN_URI"`
		DBName  string `env-required:"true" yaml:"database_name" env:"DB_NAME"`
	} `yaml:"mongodb"`
}

func NewConfig() (*Config, error) {
	cfg := &Config{}

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("config path: " + dir)

	err = cleanenv.ReadConfig(dir+"/config.yaml", cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
