package config

import (
	"fmt"
	"log"
	"os"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/thegeorgenikhil/course-platform/pkg/config"
)

type Config struct {
	config.App `yaml:"app"`
	config.Log `yaml:"logger"`

	HTTP struct {
		Port int `env-required:"true" yaml:"port" env:"HTTP_PORT"`
	} `yaml:"http"`

	InstructorService struct {
		Host string `env-required:"true" yaml:"host" env:"INSTRUCTOR_HOST"`
		Port int    `env-required:"true" yaml:"port" env:"INSTRUCTOR_PORT"`
	} `yaml:"instructor_service"`

	StudentService struct {
		Host string `env-required:"true" yaml:"host" env:"STUDENT_HOST"`
		Port int    `env-required:"true" yaml:"port" env:"STUDENT_PORT"`
	} `yaml:"student_service"`

	CourseService struct {
		Host string `env-required:"true" yaml:"host" env:"COURSE_HOST"`
		Port int    `env-required:"true" yaml:"port" env:"COURSE_PORT"`
	} `yaml:"course_service"`
}

func NewConfig() (*Config, error) {
	cfg := &Config{}

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("config path: " + dir)

	err = cleanenv.ReadConfig(dir+"/config.yaml", cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
