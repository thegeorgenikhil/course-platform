package main

import (
	"context"
	"log"

	"github.com/thegeorgenikhil/course-platform/cmd/mailer/config"
	"github.com/thegeorgenikhil/course-platform/internal/mailer/app"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatalln("not able to load config: ", err)
	}

	a := app.New(cfg)

	if err = a.RunWorker(context.Background()); err != nil {
		log.Fatalln("app run failed: ", err)
	}
}
