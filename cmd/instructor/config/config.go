package config

import (
	"fmt"
	"log"
	"os"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/thegeorgenikhil/course-platform/pkg/config"
)

type Config struct {
	config.App  `yaml:"app"`
	config.GRPC `yaml:"grpc"`
	config.Log  `yaml:"logger"`
	config.JWT  `yaml:"jwt"`

	Instructor struct {
		Name  string `env-required:"true" yaml:"name" env:"INSTRUCTOR_NAME"`
		Email string `env-required:"true" yaml:"email" env:"INSTRUCTOR_EMAIL"`
	} `yaml:"instructor"`

	CourseService struct {
		Host string `env-required:"true" yaml:"host" env:"COURSE_HOST"`
		Port int    `env-required:"true" yaml:"port" env:"COURSE_PORT"`
	} `yaml:"course_service"`

	Postgres struct {
		ConnStr string `env-required:"true" yaml:"connection_uri" env:"DB_CONN_URI"`
	} `yaml:"postgres"`

	RabbitMQ struct {
		ConnStr string `env-required:"true" yaml:"connection_uri" env:"RABBITMQ_CONN_URI"`
	} `yaml:"rabbitmq"`
}

func NewConfig() (*Config, error) {
	cfg := &Config{}

	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("config path: " + dir)

	err = cleanenv.ReadConfig(dir+"/config.yaml", cfg)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
