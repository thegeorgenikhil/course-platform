package stripe

import (
	"github.com/stripe/stripe-go/v76"
	"github.com/stripe/stripe-go/v76/checkout/session"
)

type StripeClient struct {
	redirectUrl       string
	secretKey string
}

func NewStripeClient(redirectUrl string, secretKey string) *StripeClient {
	return &StripeClient{
		redirectUrl:       redirectUrl,
		secretKey: secretKey,
	}
}

func (s *StripeClient) GetFrontendURL() string {
	return s.redirectUrl
}

func (s *StripeClient) CreateCheckoutSession(productName string, productPrice int64, sessionMetada map[string]string, successURL string, cancelURL string) (*stripe.CheckoutSession, error) {
	stripe.Key = s.secretKey

	lineItems := []*stripe.CheckoutSessionLineItemParams{
		{
			PriceData: &stripe.CheckoutSessionLineItemPriceDataParams{
				Currency: stripe.String(string(stripe.CurrencyINR)),
				ProductData: &stripe.CheckoutSessionLineItemPriceDataProductDataParams{
					Name: stripe.String(productName),
				},
				UnitAmount: stripe.Int64(productPrice * 100),
			},
			Quantity: stripe.Int64(1),
		},
	}

	params := &stripe.CheckoutSessionParams{
		Metadata: sessionMetada,
		PaymentMethodTypes: stripe.StringSlice([]string{
			"card",
		}),
		LineItems:  lineItems,
		Mode:       stripe.String(string(stripe.CheckoutSessionModePayment)),
		SuccessURL: stripe.String(successURL),
		CancelURL:  stripe.String(cancelURL),
	}

	session, err := session.New(params)
	if err != nil {
		return nil, err
	}

	return session, nil
}
