package jwt

import (
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	AccessTokenDuration           = time.Hour * 24
	UserVerificationTokenDuration = time.Hour * 1
	PasswordResetTokenDuration    = time.Hour * 1
)

var (
	ErrInvalidToken = errors.New("token is invalid")
	ErrExpiredToken = errors.New("token has expired")
)

// Claims contains the email and the standard claims for the jwt
type Claims struct {
	Email string `json:"email"`
	jwt.StandardClaims
}

type JWTGenerator struct {
	secretKey string
}

func NewJWTGenerator(secretKey string) *JWTGenerator {
	return &JWTGenerator{
		secretKey: secretKey,
	}
}

// GenerateToken creates a new token by taking in the user email and secret
func (j *JWTGenerator) GenerateToken(email string, t time.Duration) (string, error) {
	claims := &Claims{
		Email: email,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Local().Add(t).Unix(),
		},
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(j.secretKey))

	if err != nil {
		return "", err
	}

	return token, nil
}

// ValidateToken returns the Claims struct if the given token is a valid token, error if not
func (j *JWTGenerator) ValidateToken(signedToken string) (*Claims, error) {
	token, err := jwt.ParseWithClaims(
		signedToken,
		&Claims{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(j.secretKey), nil
		},
	)

	if err != nil {
		verr, ok := err.(*jwt.ValidationError)
		if ok && errors.Is(verr.Inner, ErrExpiredToken) {
			return nil, ErrExpiredToken
		}
		return nil, ErrInvalidToken
	}

	payload, ok := token.Claims.(*Claims)
	if !ok {
		return nil, ErrInvalidToken
	}

	return payload, nil
}
