package logger

import (
	"fmt"
	"path"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"
)

// LoggerConfig holds configuration options for the logger.
type LoggerConfig struct {
	Level string // Logging level (e.g., "debug", "info", "warn", "error")
}

// New initializes the logger with the provided configuration.
func New(config LoggerConfig) *log.Logger {
	appLogger := log.New()

	appLogger.SetLevel(parseLogLevel(config.Level))
	appLogger.SetReportCaller(true)

	appLogger.SetFormatter(&log.TextFormatter{
		ForceColors:     true,
		DisableColors:   false,
		TimestampFormat: "2006-01-02 15:04:05",
		FullTimestamp:   true,
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			s := strings.Split(f.Function, ".")
			funcname := s[len(s)-1]
			line := f.Line
			_, filename := path.Split(f.File)
			return "[" + funcname + "() Line: " + fmt.Sprintf("%d", line) + "]", "[" + filename + "]"
		},
	})

	appLogger.Infoln("Logger initialized")

	return appLogger
}

// parseLogLevel converts a string level to the corresponding logrus level.
func parseLogLevel(level string) log.Level {
	switch level {
	case "debug":
		return log.DebugLevel
	case "info":
		return log.InfoLevel
	case "warn":
		return log.WarnLevel
	case "error":
		return log.ErrorLevel
	default:
		return log.InfoLevel
	}
}
