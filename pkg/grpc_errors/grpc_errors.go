package grpcErrors

import (
	"net/http"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	SystemErrMessage    = "System is facing some issues. Please try again later."
	InvalidTokenMessage = "Token has expired or is invalid"
)

var (
	ErrInternal     = status.Error(codes.Internal, SystemErrMessage)
	ErrInvalidToken = status.Error(codes.Unauthenticated, InvalidTokenMessage)
)

func GetHttpStatusCodeFromGrpcErrorCode(code codes.Code) int {
	switch code {
	case codes.OK:
		return http.StatusOK
	case codes.Canceled:
		return http.StatusInternalServerError
	case codes.Unknown:
		return http.StatusInternalServerError
	case codes.InvalidArgument:
		return http.StatusBadRequest
	case codes.DeadlineExceeded:
		return http.StatusGatewayTimeout
	case codes.NotFound:
		return http.StatusNotFound
	case codes.AlreadyExists:
		return http.StatusConflict
	case codes.PermissionDenied:
		return http.StatusForbidden
	case codes.ResourceExhausted:
		return http.StatusTooManyRequests
	case codes.FailedPrecondition:
		return http.StatusBadRequest
	case codes.Aborted:
		return http.StatusConflict
	case codes.OutOfRange:
		return http.StatusBadRequest
	case codes.Unimplemented:
		return http.StatusNotImplemented
	case codes.Internal:
		return http.StatusInternalServerError
	case codes.Unavailable:
		return http.StatusServiceUnavailable
	case codes.DataLoss:
		return http.StatusInternalServerError
	case codes.Unauthenticated:
		return http.StatusUnauthorized
	default:
		return http.StatusInternalServerError
	}
}

func GenerateFromError(err error) error {
	st, ok := status.FromError(err)
	if !ok {
		return ErrInternal
	}

	return status.Error(st.Code(), st.Message())
}
