package config

type App struct {
	Name    string `env-required:"true" yaml:"name" env:"APP_NAME"`
	Version string `env-required:"true" yaml:"version" env:"APP_VERSION"`
}

type GRPC struct {
	Port string `env-required:"true" yaml:"port" env:"GRPC_PORT"`
}

type Log struct {
	Level string `env-required:"true" yaml:"log_level" env:"LOG_LEVEL"`
}

type JWT struct {
	JWTSecret string `env-required:"true" yaml:"jwt_secret" env:"JWT_SECRET"`
}

type MailServer struct {
	Host     string `env-required:"true" yaml:"host" env:"EMAIL_SERVER_HOST"`
	Port     int    `env-required:"true" yaml:"port" env:"EMAIL_SERVER_PORT"`
	Username string `env-required:"true" yaml:"username" env:"EMAIL_SERVER_USERNAME"`
	Password string `env-required:"true" yaml:"password" env:"EMAIL_SERVER_PASSWORD"`
	MailFrom string `env-required:"true" yaml:"mail_from" env:"EMAIL_FROM_ADDRESS"`
}