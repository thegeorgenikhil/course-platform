package consumer

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

type Consumer struct {
	amqpConn *amqp.Connection
	amqpChan *amqp.Channel
	logger   *logrus.Logger
	delivery <-chan amqp.Delivery
}

func NewConsumer(l *logrus.Logger, amqpConn *amqp.Connection) (*Consumer, error) {
	ch, err := amqpConn.Channel()
	if err != nil {
		return nil, err
	}

	c := &Consumer{
		amqpConn: amqpConn,
		amqpChan: ch,
		logger:   l,
	}

	return c, nil
}

func (c *Consumer) Consume(exchangeName string, queueName string, routingKeys []string) error {
	err := configureExchange(c, exchangeName)
	if err != nil {
		return err
	}

	q, err := configureQueue(c, queueName)
	if err != nil {
		return err
	}

	err = bindQueue(c, q.Name, routingKeys, exchangeName)
	if err != nil {
		return err
	}

	delivery, err := c.amqpChan.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	if err != nil {
		return err
	}

	c.delivery = delivery

	return nil
}

func (c *Consumer) GetDelivery() <-chan amqp.Delivery {
	return c.delivery
}

func configureExchange(c *Consumer, exchangeName string) error {
	err := c.amqpChan.ExchangeDeclare(
		exchangeName,        // name
		amqp.ExchangeDirect, // type
		true,                // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)

	return err
}

func configureQueue(c *Consumer, queueName string) (amqp.Queue, error) {
	q, err := c.amqpChan.QueueDeclare(
		queueName, // name
		true,      // durable
		false,     // delete when unused
		true,      // exclusive
		false,     // no-wait
		nil,       // arguments
	)

	return q, err
}

func bindQueue(c *Consumer, queueName string, routingKeys []string, exchangeName string) error {
	for _, key := range routingKeys {
		err := c.amqpChan.QueueBind(
			queueName,    // queue name
			key,          // routing key
			exchangeName, // exchange
			false,
			nil,
		)
		if err != nil {
			return err
		}
	}

	return nil
}
