package rabbitmq

import (
	"errors"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

const (
	_retryTimes = 5
	_backOffSec = 10
)

var ErrCannotConnectRabbitMQ = errors.New("cannot connect to rabbitmq")

func NewRabbitMQConn(rabbitMQURL string, l *logrus.Logger) (*amqp.Connection, error) {
	var (
		amqpConn *amqp.Connection
		counts   int64
	)

	for {
		connection, err := amqp.Dial(rabbitMQURL)
		if err != nil {
			l.Errorln("failed to connect to RabbitMQ: ", err, " ", rabbitMQURL)
			counts++
		} else {
			amqpConn = connection
			break
		}

		if counts > _retryTimes {
			l.Errorf("failed to connect to RabbitMQ even after %d retries\n", _retryTimes)
			return nil, ErrCannotConnectRabbitMQ
		}

		l.Errorln("retrying to connect to RabbitMQ in ", _backOffSec, " seconds")
		time.Sleep(_backOffSec * time.Second)

		continue
	}

	l.Infoln("connected to rabbitmq 📫🐇")

	return amqpConn, nil
}
