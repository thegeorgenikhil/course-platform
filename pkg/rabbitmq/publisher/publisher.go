package publisher

import (
	"context"

	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

const (
	_publishMandatory = false
	_publishImmediate = false
)

type Publisher struct {
	exchangeName string
	amqpConn     *amqp.Connection
	amqpChan     *amqp.Channel
	logger       *logrus.Logger
}

func NewPublisher(l *logrus.Logger, exchangeName string, amqpConn *amqp.Connection) (*Publisher, error) {
	ch, err := amqpConn.Channel()
	if err != nil {
		return nil, err
	}

	p := &Publisher{
		exchangeName: exchangeName,
		amqpConn:     amqpConn,
		amqpChan:     ch,
		logger:       l,
	}

	return p, nil
}

func (p *Publisher) DeclareExchange() error {
	err := p.amqpChan.ExchangeDeclare(
		p.exchangeName,      // name
		amqp.ExchangeDirect, // type
		true,                // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)

	if err != nil {
		return err
	}

	return nil
}

func (p *Publisher) PublishWithContext(ctx context.Context, routingKey string, body []byte, contentType string) error {
	err := p.amqpChan.PublishWithContext(ctx,
		p.exchangeName,
		routingKey,
		_publishMandatory,
		_publishImmediate,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  contentType,
			Body:         body,
		},
	)

	if err != nil {
		return err
	}

	return nil
}
