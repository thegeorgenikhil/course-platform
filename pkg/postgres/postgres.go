package postgres

import (
	"database/sql"
	"time"

	_ "github.com/lib/pq" // need this import for postgres to work!
	"github.com/sirupsen/logrus"
)

const (
	_defaultConnAttempts = 10
	_defaultConnTimeout  = time.Second * 2
)

type postgres struct {
	connStr      string
	connAttempts int
	connTimeout  time.Duration
	logger       *logrus.Logger
	db           *sql.DB
}

// just for the compile checks to make sure that postgres implements DBEngine
var _ DBEngine = (*postgres)(nil)

func NewPostgres(logger *logrus.Logger, connStr string) *postgres {
	pg := &postgres{
		logger:       logger,
		connAttempts: _defaultConnAttempts,
		connTimeout:  _defaultConnTimeout,
		connStr:      connStr,
	}

	return pg
}

func (p *postgres) GetDB() *sql.DB {
	return p.db
}

func (p *postgres) Configure(opts ...Option) DBEngine {
	for _, opt := range opts {
		opt(p)
	}

	return p
}

func (p *postgres) Connect() error {
	p.logger.Infoln("starting to connect with postgres...")

	var err error
	for p.connAttempts > 0 {
		p.db, err = sql.Open("postgres", p.connStr)
		if err == nil {
			break
		}

		p.logger.Infof("postgres is trying to connect, attempts left: %d\n", p.connAttempts)

		time.Sleep(p.connTimeout)

		p.connAttempts--
	}

	if err != nil {
		p.logger.Infoln("failed to connect to postgres: ", err)
		return err
	}

	err = p.db.Ping()
	if err != nil {
		p.logger.Errorln("postgres ping failed: ", err)
		return err
	}

	p.logger.Infoln("connected to postgresdb 🎉")

	return nil
}

func (p *postgres) Close() error {
	return p.db.Close()
}
