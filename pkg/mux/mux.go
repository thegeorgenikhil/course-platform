package mux

import (
	"crypto/rsa"
	"encoding/base64"
	"time"

	"github.com/golang-jwt/jwt"
	muxgo "github.com/muxinc/mux-go"
)

type MuxClient struct {
	client        *muxgo.APIClient
	jwtSigningKey *rsa.PrivateKey
	signingKeyId  string
}

func NewMuxClient(tokenId string, tokenSecret string, signingKeyId string, signingKey string) (*MuxClient, error) {
	cl := muxgo.NewAPIClient(
		muxgo.NewConfiguration(
			muxgo.WithBasicAuth(tokenId, tokenSecret),
		),
	)

	decodedKey, err := base64.StdEncoding.DecodeString(signingKey)
	if err != nil {
		return nil, err
	}

	jwtSigningKey, err := jwt.ParseRSAPrivateKeyFromPEM(decodedKey)
	if err != nil {
		return nil, err
	}

	return &MuxClient{
		client:        cl,
		jwtSigningKey: jwtSigningKey,
		signingKeyId:  signingKeyId,
	}, nil
}

func (m *MuxClient) UploadAsset(passthrough string) (muxgo.UploadResponse, error) {
	car := muxgo.CreateAssetRequest{PlaybackPolicy: []muxgo.PlaybackPolicy{muxgo.SIGNED}, Passthrough: passthrough}
	cur := muxgo.CreateUploadRequest{NewAssetSettings: car, Timeout: 3600, CorsOrigin: "*"} // TODO: Change cors origin
	u, err := m.client.DirectUploadsApi.CreateDirectUpload(cur)

	return u, err
}

func (m *MuxClient) GetSignedToken(playbackId string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, jwt.MapClaims{
		"sub": playbackId,
		"aud": "v",
		"exp": time.Now().Add(time.Minute * 15).Unix(),
		"kid": m.signingKeyId,
	})

	tokenString, err := token.SignedString(m.jwtSigningKey)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}
