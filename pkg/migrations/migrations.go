package migrations

import (
	"errors"
	"fmt"
	"time"

	"github.com/golang-migrate/migrate/v4"
	"github.com/sirupsen/logrus"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

const (
	_defaultAttempts   = 5
	_defaultTimeout    = 2 * time.Second
	_migrationFilePath = "/db/migrations"
)

var (
	ErrPostgresConnFailed = errors.New("not able to connect to postgres for migrations")
	ErrMigrationUp        = errors.New("migrations up error")
)

type migrations struct {
	logger *logrus.Logger
}

func NewMigrate(logger *logrus.Logger) *migrations {
	return &migrations{
		logger: logger,
	}
}

func (mgs *migrations) Migrate(dbConnStr string) error {
	if len(dbConnStr) == 0 {
		mgs.logger.Fatalln("database connection string cannot be emtpy")
	}

	var (
		attempts = _defaultAttempts
		timeout  = _defaultTimeout
		err      error
		mg       *migrate.Migrate
	)

	for attempts > 0 {
		migrationPath := fmt.Sprintf("file://%s", _migrationFilePath)

		mg, err = migrate.New(migrationPath, dbConnStr)
		if err == nil {
			break
		}

		mgs.logger.Infoln("trying to connect to postgres for migrations, attempts left: ", attempts)
		time.Sleep(timeout)
		attempts--
	}

	if err != nil {
		mgs.logger.Errorln(err)
		return ErrPostgresConnFailed
	}

	err = mg.Up()
	defer mg.Close()

	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		mgs.logger.Errorln(err)
		return ErrMigrationUp
	}

	if errors.Is(err, migrate.ErrNoChange) {
		mgs.logger.Infoln("no changes while running migrations: success")
		return nil
	}

	mgs.logger.Infoln("migrations done: success")

	return nil
}
