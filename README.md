# Go Course Platform

A Go-based self-deployable course platform with made using microservices architecture. The project is intended for single instructor wanting to sell their courses online.

The services use [gRPC](https://grpc.io/docs/languages/go/basics/) for communication with each other.

Steps to run the project in both local and production environments are mentioned below.

# Table of Contents

- [Diagram of the Architecture](#diagram-of-the-architecture)
- [Key Functionality](#key-functionality)
- [Full List of what has been used](#full-list-of-what-has-been-used)
- [Required Installations](#required-installations)
- [Setup](#setup)
  - [For Local Development](#for-local-development)
  - [For Production](#for-production)
- [Environment Variables](#environment-variables)
- [Folder Structure](#folder-structure)
- [Issues](#issues)

## Diagram of the Architecture

![Architecture](./diagrams/diagram.png)

## Key Functionality

- **Course Management**: An Instructor can create, update, and publish courses, adding lessons and videos as necessary.
- **Enrollment and Purchasing**: Students can enroll in courses and make purchases securely.
- **Authentication and Profile Management**: Users can register, login, and manage their profiles, ensuring secure access to the platform.
- **Content Delivery**: Lessons and videos are efficiently delivered to users, providing seamless learning experiences.
- **Inter-Service Communication**: Services communicate with each other using defined RPC (Remote Procedure Call) methods, ensuring smooth integration and operation.

## Full List of what has been used:

- [Go](https://go.dev/) for creating microservices
- [gRPC](https://grpc.io/docs/languages/go/basics/) for communication between microservices
- [RabbitMQ](https://rabbitmq.com/documentation.html) for event-driven architecture
- [MongoDB](https://github.com/mongodb/mongo-go-driver) for storing course data and transaction details
- [Postgres](https://github.com/lib/pq) for storing user and instructor details
- [Stripe](https://stripe.com/docs/payments/checkout) for payment processing
- [Mux](https://docs.mux.com/) for video uploads and streaming
- [SendInBlue](https://www.brevo.com/en/) for email communication
- [Docker](https://docs.docker.com/) for containerization
- [SvelteKit](https://kit.svelte.dev/) for both the instructor and student front-end

## Required Installations

1. Go [Install here](https://go.dev/doc/install)

2. Docker [Install here(ubuntu)](https://docs.docker.com/engine/install/ubuntu/)

3. Protobuf Compiler

```bash
go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28

go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2

export PATH="$PATH:$(go env GOPATH)/bin"
```

4. Stripe CLI [Install here(For development only)](https://stripe.com/docs/stripe-cli)

5. Ngrok [Install here(For development only)](https://ngrok.com/download)

## Setup

### For Local Development

1. Start the services (RabbitMQ, Postgres, MongoDB) using the `docker-compose.dev.services.yml` file

```bash
make dev_up_services_build

# If you want to remove the existing volumes and start fresh
make dev_up_services_build rm_vol=true
```

2. Start the microservices using the `docker-compose.dev.microservices.yml` file

```bash
make dev_up_microservices_build
```

3. Use the `stripe listen` command to listen for events from Stripe when in local development

```bash
make stripe_listen
```

4. To receive the webhooks from Mux about video uploads, install `ngrok` and run the following command

```bash
make mux_ngrok
# After running this command, copy the https url and create a new webhook in the Mux dashboard
# Example: https://19c1-152-58-166-219.ngrok-free.app
# The webhook endpoint is /course/mux/webhook
# The webhook url should be https://19c1-152-58-166-219.ngrok-free.app/course/mux/webhook
```

5. Start the admin frontend

```bash
# [ONE-TIME] Install the dependencies
make dev_admin_frontend_install 

# Start the admin frontend
make dev_admin_frontend
```

6. Start the student frontend

```bash
# [ONE-TIME] Install the dependencies
make dev_frontend_install

# Start the student frontend
make dev_frontend
```

7. To stop all the running services and microservices

```bash
# Stop the services
make dev_down_services

# Stop the microservices
make dev_down_microservices
```

### For Production

1. Start all the services and microservices using the `docker-compose.yml` file

```bash
make prod_up_build
```

2. To stop all the running services and microservices

```bash
make prod_down
```

## Environment Variables

Environment variables are stored in `.env.production` file and is passed to Docker.

Create a copy of `.env.example` renaming it to `.env.production` and fill the environment variables according to documentation.

| Variable Name           | Description                                                                 |
| ----------------------- | --------------------------------------------------------------------------- |
| `STRIPE_SECRET_KEY`     | Stripe secret key creating checkout sessions                                |
| `STRIPE_WEBHOOK_SECRET` | Stripe webhook secret for verifying webhooks                                |
| `MUX_TOKEN_ID`          | Mux token id for video uploads and streaming                                |
| `MUX_TOKEN_SECRET`      | Mux token secret for video uploads and streaming                            |
| `MUX_SIGNING_KEY`       | Mux Signing key is used to generate valid JWTs for accessing private videos |
| `MUX_SIGNING_KEY_ID`    | Mux Signing key id                                                          |

## Folder Structure

```sh
├── admin-frontend # Contains the SveteKit frontend for the instructor
├── cmd
│   ├── api-gateway # Entry point for the API Gateway
│   ├── course # Entry point for the Course service
│   ├── instructor # Entry point for the Instructor service
│   ├── mailer # Entry point for the Mailer service
│   └── student # Entry point for the Student service
├── common # Contains common types and enums used across the services
├── db
│   └── migrations # Contains the migrations for the Postgres database
├── diagrams # Contains the diagrams for the architecture
├── docker # Contains the Dockerfiles for the services
├── docker-compose.dev.microservices.yml # Docker compose file for running the microservices in development
├── docker-compose.dev.services.yml # Docker compose file for running the services(Postgres, RabbitMQ, MongoDB) in development
├── docker-compose.yml # Docker compose file for running the services and microservices in production
├── frontend # Contains the SveteKit frontend for the student
├── go.mod # Go module file
├── go.sum # Go sum file
├── internal
│   ├── api-gateway # Contains all the logic for the API Gateway
│   ├── course # Contains all the logic for the Course service
│   ├── instructor # Contains all the logic for the Instructor service
│   ├── mailer # Contains all the logic for the Mailer service
│   └── student # Contains all the logic for the Student service
├── Makefile # Contains all the commands for running the services and microservices
├── pkg
│   ├── bcrypt # bcrypt package for hashing passwords
│   ├── config # config package for coniguration management
│   ├── jwt # jwt package for generating and verifying jwt tokens
│   ├── logger # logger package for logging
│   ├── migrations # migrations package for running migrations
│   ├── mux # mux package for handling video uploads and streaming
│   ├── postgres # postgres package for handling postgres database
│   ├── rabbitmq # rabbitmq package for handling rabbitmq
│   └── stripe # stripe package for handling stripe payments
├── proto
│   ├── course.proto # Contains the protobuf definition for the course service
│   ├── instructor.proto # Contains the protobuf definition for the instructor service
│   └── student.proto # Contains the protobuf definition for the student service
├── README.md
└── utils # Contains utility functions for error handling and response handling
```