package student

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/api-gateway/config"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type StudentServiceClient struct {
	Client pb.StudentServiceClient
	Logger *logrus.Logger
}

func NewStudentServiceClient(c *config.Config, l *logrus.Logger) (*StudentServiceClient, error) {
	instructionServiceURL := fmt.Sprintf("%s:%d", c.StudentService.Host, c.StudentService.Port)
	conn, err := grpc.Dial(instructionServiceURL, grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		return nil, err
	}

	return &StudentServiceClient{
		Client: pb.NewStudentServiceClient(conn),
		Logger: l,
	}, nil
}
