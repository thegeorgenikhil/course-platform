package student

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/api-gateway/config"
	"github.com/thegeorgenikhil/course-platform/internal/api-gateway/services/student/routes"
)

func RegisterStudentRoutes(r *gin.Engine, cfg *config.Config, l *logrus.Logger) {
	svc, err := NewStudentServiceClient(cfg, l)
	if err != nil {
		l.Fatalln("not able to create student service client: ", err)
	}

	routes := r.Group("/student")
	routes.POST("/register", svc.RegisterStudent)
	routes.POST("/resend-verification-mail", svc.ResendVerificationEmail)
	routes.POST("/verify-email", svc.VerifyEmail)

	routes.POST("/login", svc.LoginStudent)

	routes.GET("/info", svc.GetStudentInfo)

	routes.POST("/forgot-password", svc.ForgotPassword)
	routes.POST("/reset-password", svc.ResetPassword)

	routes.PUT("/", svc.UpdateStudent)
	routes.DELETE("/", svc.DeleteStudent)

	routes.GET("/courses", svc.GetAllCourses)
	routes.GET("/courses/:id", svc.GetCourseById)

	routes.POST("/course/:courseId/l/:lessonId/v/:videoId", svc.GetVideoURL)

	routes.POST("/buy-course/:id", svc.BuyCourse)

	routes.GET("/my-courses", svc.GetEnrolledCourses)
}

func (svc *StudentServiceClient) RegisterStudent(ctx *gin.Context) {
	routes.RegisterStudent(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) ResendVerificationEmail(ctx *gin.Context) {
	routes.ResendVerificationEmail(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) VerifyEmail(ctx *gin.Context) {
	routes.VerifyEmail(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) LoginStudent(ctx *gin.Context) {
	routes.LoginStudent(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) GetStudentInfo(ctx *gin.Context) {
	routes.GetStudentInfo(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) ForgotPassword(ctx *gin.Context) {
	routes.ForgotPassword(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) ResetPassword(ctx *gin.Context) {
	routes.ResetPassword(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) UpdateStudent(ctx *gin.Context) {
	routes.UpdateStudent(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) DeleteStudent(ctx *gin.Context) {
	routes.DeleteStudent(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) GetAllCourses(ctx *gin.Context) {
	routes.GetAllCourses(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) GetCourseById(ctx *gin.Context) {
	routes.GetCourseById(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) GetVideoURL(ctx *gin.Context) {
	routes.GetVideoURL(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) BuyCourse(ctx *gin.Context) {
	routes.BuyCourse(ctx, svc.Logger, svc.Client)
}

func (svc *StudentServiceClient) GetEnrolledCourses(ctx *gin.Context) {
	routes.GetEnrolledCourses(ctx, svc.Logger, svc.Client)
}
