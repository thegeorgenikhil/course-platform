package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type RegisterStudentRequest struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func RegisterStudent(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	b := &RegisterStudentRequest{}

	if err := ctx.ShouldBindJSON(b); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := cl.RegisterStudent(ctx, &pb.RegisterStudentRequest{
		Name:     b.Name,
		Email:    b.Email,
		Password: b.Password,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
