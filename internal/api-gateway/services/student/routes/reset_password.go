package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type ResetPasswordRequest struct {
	Token    string `json:"token"`
	Password string `json:"password"`
}

func ResetPassword(ctx *gin.Context, l *logrus.Logger, client pb.StudentServiceClient) {
	var req ResetPasswordRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := client.ResetStudentPassword(ctx, &pb.ResetStudentPasswordRequest{
		ResetPasswordToken: req.Token,
		Password:           req.Password,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
