package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

func DeleteStudent(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	res, err := cl.DeleteStudent(ctx, &pb.DeleteStudentRequest{
		Token: token,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
