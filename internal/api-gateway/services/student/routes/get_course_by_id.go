package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

func GetCourseById(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token != "" {
		token = token[7:] // Remove "Bearer" from token
	}

	id := ctx.Param("id")
	if id == "" {
		utils.HandleError(ctx, http.StatusBadRequest, "invalid id")
		return
	}

	res, err := cl.GetCourseById(ctx, &pb.GetCourseByIdRequest{
		CourseId: id,
		Token:    token,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
