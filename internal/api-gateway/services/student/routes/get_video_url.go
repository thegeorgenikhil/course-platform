package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

func GetVideoURL(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	courseId := ctx.Param("courseId")
	lessonId := ctx.Param("lessonId")
	videoId := ctx.Param("videoId")

	if courseId == "" || lessonId == "" || videoId == "" {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid id")
		return
	}

	res, err := cl.GetVideoInfo(ctx, &pb.GetVideoInfoRequest{
		CourseId: courseId,
		LessonId: lessonId,
		VideoId:  videoId,
		Token:    token,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
