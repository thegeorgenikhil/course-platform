package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

func BuyCourse(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	id := ctx.Param("id")
	if id == "" {
		utils.HandleError(ctx, http.StatusBadRequest, "invalid id")
		return
	}

	res, err := cl.BuyCourse(ctx, &pb.BuyCourseRequest{
		Token:    token,
		CourseId: id,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
