package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type ResendVerificationEmailRequest struct {
	Email string `json:"email"`
}

func ResendVerificationEmail(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	b := &ResendVerificationEmailRequest{}

	if err := ctx.ShouldBindJSON(b); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := cl.ResendVerificationEmail(ctx, &pb.ResendVerificationEmailRequest{
		Email: b.Email,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
