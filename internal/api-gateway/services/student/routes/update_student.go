package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type UpdateStudentRequest struct {
	Name string `json:"name"`
}

func UpdateStudent(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	b := &UpdateStudentRequest{}

	if err := ctx.ShouldBindJSON(b); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := cl.UpdateStudent(ctx, &pb.UpdateStudentRequest{
		Token: token,
		Name:  b.Name,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
