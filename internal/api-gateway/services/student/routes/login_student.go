package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type LoginStudentRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func LoginStudent(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	b := &LoginStudentRequest{}

	if err := ctx.ShouldBindJSON(b); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := cl.LoginStudent(ctx, &pb.LoginStudentRequest{
		Email:    b.Email,
		Password: b.Password,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
