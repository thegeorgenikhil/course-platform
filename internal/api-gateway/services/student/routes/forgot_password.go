package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type ForgotPasswordRequest struct {
	Email string `json:"email"`
}

func ForgotPassword(ctx *gin.Context, l *logrus.Logger, client pb.StudentServiceClient) {
	var req ForgotPasswordRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := client.SendPasswordResetToken(ctx, &pb.SendPasswordResetTokenRequest{
		Email: req.Email,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
