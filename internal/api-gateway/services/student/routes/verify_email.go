package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type VerifyEmailRequest struct {
	Email            string `json:"email"`
	VerificationCode string `json:"verification_code"`
}

func VerifyEmail(ctx *gin.Context, l *logrus.Logger, cl pb.StudentServiceClient) {
	b := &VerifyEmailRequest{}

	if err := ctx.ShouldBindJSON(b); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := cl.VerifyStudentEmail(ctx, &pb.VerifyStudentEmailRequest{
		VerificationCode: b.VerificationCode,
	})

	if err != nil {
		l.Error("Error while getting response from student service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
