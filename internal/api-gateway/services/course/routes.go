package course

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/api-gateway/config"
	"github.com/thegeorgenikhil/course-platform/internal/api-gateway/services/course/routes"
)

func RegisterCourseRoutes(r *gin.Engine, cfg *config.Config, l *logrus.Logger) {
	svc, err := NewCourseServiceClient(cfg, l)
	if err != nil {
		l.Fatalln("not able to create course service client: ", err)
	}

	routes := r.Group("/course")

	routes.POST("/stripe/webhook", svc.StripeWebhook)
	routes.POST("/mux/webhook", svc.MuxWebhook)
}

func (svc *CourseServiceClient) StripeWebhook(ctx *gin.Context) {
	routes.StripeWebhook(ctx, svc.Logger, svc.Client)
}

func (svc *CourseServiceClient) MuxWebhook(ctx *gin.Context) {
	routes.MuxWebhook(ctx, svc.Logger, svc.Client)
}
