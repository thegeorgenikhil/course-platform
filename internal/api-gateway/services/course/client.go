package course

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/api-gateway/config"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type CourseServiceClient struct {
	Client pb.CourseServiceClient
	Logger *logrus.Logger
}

func NewCourseServiceClient(c *config.Config, l *logrus.Logger) (*CourseServiceClient, error) {
	courseServiceURL := fmt.Sprintf("%s:%d", c.CourseService.Host, c.CourseService.Port)
	conn, err := grpc.Dial(courseServiceURL, grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		return nil, err
	}

	return &CourseServiceClient{
		Client: pb.NewCourseServiceClient(conn),
		Logger: l,
	}, nil
}
