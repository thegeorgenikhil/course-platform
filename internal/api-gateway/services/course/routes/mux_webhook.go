package routes

import (
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

func MuxWebhook(ctx *gin.Context, l *logrus.Logger, cl pb.CourseServiceClient) {
	payload, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		l.Errorln("Error reading request body: ", err)
		utils.HandleError(ctx, http.StatusBadRequest, "Error reading request body")
	}

	_, err = cl.MuxWebhook(ctx, &pb.MuxWebhookRequest{
		Payload: payload,
	})

	if err != nil {
		l.Errorln("Error calling MuxWebhook: ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.Status(http.StatusOK)
}
