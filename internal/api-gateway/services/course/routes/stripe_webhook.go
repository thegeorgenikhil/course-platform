package routes

import (
	"io"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

func StripeWebhook(ctx *gin.Context, l *logrus.Logger, cl pb.CourseServiceClient) {
	payload, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		l.Errorln("Error reading request body: ", err)
		utils.HandleError(ctx, http.StatusBadRequest, "Error reading request body")
	}

	stripeSignature := ctx.Request.Header.Get("Stripe-Signature")

	_, err = cl.StripeWebhook(ctx, &pb.StripeWebhookRequest{
		Payload:         payload,
		StripeSignature: stripeSignature,
	})

	if err != nil {
		l.Errorln("Error calling StripeWebhook: ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.Status(http.StatusOK)
}
