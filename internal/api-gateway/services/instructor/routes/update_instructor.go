package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type UpdateInstructorRequest struct {
	Name string `json:"name"`
	Bio  string `json:"bio"`
}

func UpdateInstructor(ctx *gin.Context, l *logrus.Logger, cl pb.InstructorServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	b := &UpdateInstructorRequest{}

	if err := ctx.ShouldBindJSON(b); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := cl.UpdateInstructor(ctx, &pb.UpdateInstructorRequest{
		Token: token,
		Name:  b.Name,
		Bio:   b.Bio,
	})

	if err != nil {
		l.Error("Error while getting response from instructor service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
