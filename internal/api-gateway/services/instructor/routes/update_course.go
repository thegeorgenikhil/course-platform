package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type UpdateCourseRequest struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Category    string `json:"category"`
	Price       uint64 `json:"price"`
}

func UpdateCourse(ctx *gin.Context, l *logrus.Logger, cl pb.InstructorServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	id := ctx.Param("courseId")

	if id == "" {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid id")
		return
	}

	var req UpdateCourseRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := cl.UpdateCourse(ctx, &pb.UpdateCourseRequest{
		Token:       token,
		CourseId:    id,
		Title:       req.Title,
		Description: req.Description,
		Category:    req.Category,
		Price:       req.Price,
	})

	if err != nil {
		l.Error("Error while getting response from instructor service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
