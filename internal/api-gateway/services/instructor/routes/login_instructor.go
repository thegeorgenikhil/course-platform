package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type LoginInstructorRequest struct {
	Email string `json:"email"`
}

func SendLoginInstructorMail(ctx *gin.Context, l *logrus.Logger, cl pb.InstructorServiceClient) {
	b := &LoginInstructorRequest{}

	if err := ctx.ShouldBindJSON(b); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := cl.SendLoginInstructorMail(ctx, &pb.SendLoginInstructorMailRequest{
		Email: b.Email,
	})

	if err != nil {
		l.Error("Error while getting response from instructor service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
