package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type PublishCourseRequest struct {
	CourseId string `json:"course_id"`
}

func PublishCourse(ctx *gin.Context, l *logrus.Logger, cl pb.InstructorServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	var req PublishCourseRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request")
		return
	}

	res, err := cl.PublishCourse(ctx, &pb.PublishCourseRequest{
		CourseId: req.CourseId,
		Token:    token,
	})

	if err != nil {
		l.Error("Error while getting response from instructor service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
