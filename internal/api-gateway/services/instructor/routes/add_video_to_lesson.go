package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

func AddVideoToLesson(ctx *gin.Context, l *logrus.Logger, cl pb.InstructorServiceClient) {
	courseId := ctx.Param("courseId")

	if courseId == "" {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid course id")
		return
	}

	lessonId := ctx.Param("lessonId")

	if lessonId == "" {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid lesson id")
		return
	}

	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	res, err := cl.AddVideoToLesson(ctx, &pb.AddVideoToLessonRequest{
		Token:    token,
		CourseId: courseId,
		LessonId: lessonId,
	})

	if err != nil {
		l.Error("Error while getting response from instructor service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
