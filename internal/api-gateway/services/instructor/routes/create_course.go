package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type CreateCourseRequest struct {
	Title       string `json:"title" binding:"required"`
	Description string `json:"description" binding:"required"`
	Category    string `json:"category" binding:"required"`
	Price       uint64 `json:"price" binding:"required"`
}

func CreateCourse(ctx *gin.Context, l *logrus.Logger, cl pb.InstructorServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	var req CreateCourseRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	res, err := cl.CreateCourse(ctx, &pb.CreateCourseRequest{
		Token:       token,
		Title:       req.Title,
		Description: req.Description,
		Category:    req.Category,
		Price:       req.Price,
	})

	if err != nil {
		l.Error("Error while getting response from instructor service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
