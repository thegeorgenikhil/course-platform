package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"github.com/thegeorgenikhil/course-platform/utils"
)

type AddLessonToCourseRequest struct {
	CourseId    string `json:"course_id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

func AddLessonToCourse(ctx *gin.Context, l *logrus.Logger, cl pb.InstructorServiceClient) {
	token := ctx.GetHeader("Authorization")
	if token == "" {
		utils.HandleError(ctx, http.StatusUnauthorized, "Unauthorized")
		return
	}

	token = token[7:] // Remove "Bearer" from token

	b := &AddLessonToCourseRequest{}

	if err := ctx.ShouldBindJSON(b); err != nil {
		utils.HandleError(ctx, http.StatusBadRequest, "Invalid request body")
		return
	}

	res, err := cl.AddLessonToCourse(ctx, &pb.AddLessonToCourseRequest{
		Token:       token,
		CourseId:    b.CourseId,
		Name:        b.Name,
		Description: b.Description,
	})

	if err != nil {
		l.Error("Error while getting response from instructor service ", err)
		utils.HandleGrpcError(ctx, err)
		return
	}

	ctx.JSON(http.StatusOK, res)
}
