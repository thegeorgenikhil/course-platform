package instructor

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/api-gateway/config"
	"github.com/thegeorgenikhil/course-platform/internal/api-gateway/services/instructor/routes"
)

func RegisterInstructorRoutes(r *gin.Engine, cfg *config.Config, l *logrus.Logger) {
	svc, err := NewInstructorServiceClient(cfg, l)
	if err != nil {
		l.Fatalln("not able to create instructor service client: ", err)
	}

	routes := r.Group("/instructor")
	routes.POST("/login", svc.SendLoginInstructorMail)
	routes.POST("/authenticate", svc.AutheticateInstructor)

	routes.GET("/info", svc.GetInstructorInfo)

	routes.PUT("/", svc.UpdateInstructor)

	routes.POST("/course", svc.CreateCourse)
	routes.GET("/course", svc.GetAllCourses)

	routes.GET("/course/:courseId", svc.GetCourseById)

	routes.PUT("/course/:courseId", svc.UpdateCourse)

	routes.POST("/course/l/new", svc.AddLessonToCourse)
	routes.POST("/course/:courseId/l/:lessonId/v/new", svc.AddVideoToLesson)

	routes.POST("/course/:courseId/l/:lessonId/v/:videoId", svc.GetVideoURL)

	routes.POST("/course/publish", svc.PublishCourse)
}

func (svc *InstructorServiceClient) SendLoginInstructorMail(ctx *gin.Context) {
	routes.SendLoginInstructorMail(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) AutheticateInstructor(ctx *gin.Context) {
	routes.AuthenticateInstructor(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) GetInstructorInfo(ctx *gin.Context) {
	routes.GetInstructorInfo(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) UpdateInstructor(ctx *gin.Context) {
	routes.UpdateInstructor(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) CreateCourse(ctx *gin.Context) {
	routes.CreateCourse(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) GetAllCourses(ctx *gin.Context) {
	routes.GetAllCourses(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) GetCourseById(ctx *gin.Context) {
	routes.GetCourseById(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) UpdateCourse(ctx *gin.Context) {
	routes.UpdateCourse(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) AddLessonToCourse(ctx *gin.Context) {
	routes.AddLessonToCourse(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) AddVideoToLesson(ctx *gin.Context) {
	routes.AddVideoToLesson(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) PublishCourse(ctx *gin.Context) {
	routes.PublishCourse(ctx, svc.Logger, svc.Client)
}

func (svc *InstructorServiceClient) GetVideoURL(ctx *gin.Context) {
	routes.GetVideoURL(ctx, svc.Logger, svc.Client)
}
