package instructor

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/api-gateway/config"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type InstructorServiceClient struct {
	Client pb.InstructorServiceClient
	Logger *logrus.Logger
}

func NewInstructorServiceClient(c *config.Config, l *logrus.Logger) (*InstructorServiceClient, error) {
	instructionServiceURL := fmt.Sprintf("%s:%d", c.InstructorService.Host, c.InstructorService.Port)
	conn, err := grpc.Dial(instructionServiceURL, grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		return nil, err
	}

	return &InstructorServiceClient{
		Client: pb.NewInstructorServiceClient(conn),
		Logger: l,
	}, nil
}
