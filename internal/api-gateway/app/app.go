package app

import (
	"context"
	"fmt"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/thegeorgenikhil/course-platform/cmd/api-gateway/config"
	"github.com/thegeorgenikhil/course-platform/internal/api-gateway/services/course"
	"github.com/thegeorgenikhil/course-platform/internal/api-gateway/services/instructor"
	"github.com/thegeorgenikhil/course-platform/internal/api-gateway/services/student"
	"github.com/thegeorgenikhil/course-platform/pkg/logger"
)

type App struct {
	cfg *config.Config
}

func New(cfg *config.Config) *App {
	return &App{
		cfg: cfg,
	}
}

func (a *App) Run(ctx context.Context) error {

	l := logger.New(logger.LoggerConfig{
		Level: a.cfg.Log.Level,
	})

	l.Infoln("starting app -> name: ", a.cfg.App.Name, " version: ", a.cfg.App.Version)

	r := gin.Default()

	r.Use(cors.New(
		cors.Config{
			AllowOrigins:     []string{"*"},
			AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
			AllowHeaders:     []string{"Origin", "Content-Type", "Authorization"},
			AllowCredentials: true,
		},
	))

	l.Infof("Starting HTTP server on port: %d....\n", a.cfg.HTTP.Port)

	instructor.RegisterInstructorRoutes(r, a.cfg, l)
	student.RegisterStudentRoutes(r, a.cfg, l)
	course.RegisterCourseRoutes(r, a.cfg, l)

	return r.Run(fmt.Sprintf(":%d", a.cfg.HTTP.Port))
}
