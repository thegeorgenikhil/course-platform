package service

import (
	"context"
	"database/sql"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/common"
	"github.com/thegeorgenikhil/course-platform/internal/student/client/course"
	"github.com/thegeorgenikhil/course-platform/internal/student/repo"
	"github.com/thegeorgenikhil/course-platform/pkg/bcrypt"
	grpcErrors "github.com/thegeorgenikhil/course-platform/pkg/grpc_errors"
	"github.com/thegeorgenikhil/course-platform/pkg/jwt"
	"github.com/thegeorgenikhil/course-platform/pkg/rabbitmq/publisher"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type StudentService struct {
	logger            *logrus.Logger
	repo              *repo.StudentRepo
	jwt               *jwt.JWTGenerator
	rabbitMQPublisher *publisher.Publisher
	courseClient      *course.CourseServiceClient
}

func NewStudentService(logger *logrus.Logger, studentRepo *repo.StudentRepo, jwt *jwt.JWTGenerator, publisher *publisher.Publisher, courseClient *course.CourseServiceClient) *StudentService {
	return &StudentService{
		logger:            logger,
		repo:              studentRepo,
		jwt:               jwt,
		rabbitMQPublisher: publisher,
		courseClient:      courseClient,
	}
}

func (s *StudentService) RegisterStudent(ctx context.Context, req *pb.RegisterStudentRequest) (*pb.Empty, error) {
	exists, err := s.repo.CheckIfStudentExists(ctx, req.Email)
	if err != nil {
		s.logger.Error("Error while checking if student exists ", err)
		return nil, grpcErrors.ErrInternal
	}

	if exists {
		return nil, status.Error(codes.InvalidArgument, "Account with this email already exists")
	}

	hashedPassword, err := bcrypt.HashPassword(req.Password)
	if err != nil {
		s.logger.Error("Error while hashing password", err)
		return nil, grpcErrors.ErrInternal
	}

	verificationToken, err := s.jwt.GenerateToken(req.Email, jwt.UserVerificationTokenDuration)
	if err != nil {
		s.logger.Error("Error while generating jwt verification token ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.CreateStudent(ctx, req.Name, req.Email, hashedPassword, false, verificationToken, "")
	if err != nil {
		s.logger.Error("Error while registering student ", err)
		return nil, grpcErrors.ErrInternal
	}

	var e = common.Event{
		Name: common.RegisterStudentEventName,
		Data: common.RegisterEventPayload{
			Name:              req.Name,
			Email:             req.Email,
			VerificationToken: verificationToken,
			Subject:           common.RegisterEmailSubject,
		},
	}

	b, _ := json.Marshal(e)

	err = s.rabbitMQPublisher.PublishWithContext(ctx, common.RABBITMQ_STUDENT_MAIL_ROUTING_KEY, b, common.RABBITMQ_CONTENT_TYPE_JSON)
	if err != nil {
		s.logger.Error("Error while publishing event ", err)
		return nil, status.Error(codes.Internal, "Error while sending verification email. Please try again later")
	}

	return &pb.Empty{}, nil
}

func (s *StudentService) ResendVerificationEmail(ctx context.Context, req *pb.ResendVerificationEmailRequest) (*pb.Empty, error) {
	exists, err := s.repo.CheckIfStudentExists(ctx, req.Email)
	if err != nil {
		s.logger.Error("Error while checking if student exists ", err)
		return nil, grpcErrors.ErrInternal
	}

	if !exists {
		return nil, status.Error(codes.InvalidArgument, "Student with this email does not exist")
	}

	student, err := s.repo.GetStudentByEmail(ctx, req.Email)
	if err != nil {
		s.logger.Error("Error while getting student by email ", err)
		return nil, grpcErrors.ErrInternal
	}

	if student.IsVerified {
		return nil, status.Error(codes.InvalidArgument, "Student is already verified")
	}

	verificationToken, err := s.jwt.GenerateToken(req.Email, jwt.UserVerificationTokenDuration)
	if err != nil {
		s.logger.Error("Error while generating jwt verification token ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.UpdateStudentVerificationToken(context.Background(), req.Email, verificationToken)
	if err != nil {
		s.logger.Error("Error while updating student verification token ", err)
		return nil, grpcErrors.ErrInternal
	}

	var e = common.Event{
		Name: common.ResendStudentVerificationEventName,
		Data: common.RegisterEventPayload{
			Name:              student.Name,
			Email:             student.Email,
			VerificationToken: verificationToken,
			Subject:           common.RegisterEmailSubject,
		},
	}

	b, _ := json.Marshal(e)

	err = s.rabbitMQPublisher.PublishWithContext(ctx, common.RABBITMQ_STUDENT_MAIL_ROUTING_KEY, b, common.RABBITMQ_CONTENT_TYPE_JSON)
	if err != nil {
		s.logger.Error("Error while publishing event ", err)
		return nil, status.Error(codes.Internal, "Error while sending verification email. Please try again later")
	}

	return &pb.Empty{}, nil
}

func (s *StudentService) VerifyStudentEmail(ctx context.Context, req *pb.VerifyStudentEmailRequest) (*pb.Empty, error) {

	verifyToken, err := s.jwt.ValidateToken(req.VerificationCode)
	if err != nil {
		if err == jwt.ErrExpiredToken || err == jwt.ErrInvalidToken {
			return nil, status.Error(codes.InvalidArgument, "Verification token has expired. Please generate a new verification token")
		}

		s.logger.Error("Error while validating jwt verification token ", err)
		return nil, grpcErrors.ErrInternal
	}

	email := verifyToken.Email

	exists, err := s.repo.CheckIfStudentExists(ctx, email)
	if err != nil {
		s.logger.Error("Error while checking if student exists ", err)
		return nil, grpcErrors.ErrInternal
	}

	if !exists {
		return nil, status.Error(codes.InvalidArgument, "Account with this email does not exist")
	}

	student, err := s.repo.GetStudentByEmail(ctx, email)
	if err != nil {
		s.logger.Error("Error while getting student by email ", err)
		return nil, grpcErrors.ErrInternal
	}

	if student.IsVerified {
		return nil, status.Error(codes.InvalidArgument, "Account is already verified")
	}

	if req.VerificationCode != student.VerificationToken {
		return nil, status.Error(codes.InvalidArgument, "Wrong verification token")
	}

	err = s.repo.UpdateStudentVerificationStatus(ctx, email)
	if err != nil {
		s.logger.Error("Error while updating student verification status ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.Empty{}, nil
}

func (s *StudentService) LoginStudent(ctx context.Context, req *pb.LoginStudentRequest) (*pb.LoginStudentResponse, error) {
	exists, err := s.repo.CheckIfStudentExists(ctx, req.Email)
	if err != nil {
		s.logger.Error("Error while checking if student exists ", err)
		return nil, grpcErrors.ErrInternal
	}

	if !exists {
		return nil, status.Error(codes.InvalidArgument, "Account with this email does not exist")
	}

	student, err := s.repo.GetStudentByEmail(ctx, req.Email)
	if err != nil {
		s.logger.Error("Error while getting student by email ", err)
		return nil, grpcErrors.ErrInternal
	}

	if !student.IsVerified {
		return nil, status.Error(codes.InvalidArgument, "Account is not verified")
	}

	ok := bcrypt.VerifyPassword(student.Password, req.Password)
	if !ok {
		return nil, status.Error(codes.InvalidArgument, "Invalid password")
	}

	token, err := s.jwt.GenerateToken(req.Email, jwt.AccessTokenDuration)
	if err != nil {
		s.logger.Error("Error while generating jwt login token ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.LoginStudentResponse{
		Token: token,
	}, nil
}

func (s *StudentService) GetStudentInfo(ctx context.Context, req *pb.GetStudentInfoRequest) (*pb.GetStudentInfoResponse, error) {
	studentId, err := s.ValidateStudentToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating student token ", err)
		return nil, grpcErrors.ErrInternal
	}

	student, err := s.repo.GetStudentById(ctx, studentId)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Error(codes.InvalidArgument, "Account not found")
		}

		s.logger.Error("Error while getting student by id ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.GetStudentInfoResponse{
		Student: &pb.Student{
			Id:   student.Id,
			Name: student.Name,
		},
	}, nil
}

func (s *StudentService) SendPasswordResetToken(ctx context.Context, req *pb.SendPasswordResetTokenRequest) (*pb.Empty, error) {
	exists, err := s.repo.CheckIfStudentExists(ctx, req.Email)
	if err != nil {
		s.logger.Error("Error while checking if student exists ", err)
		return nil, grpcErrors.ErrInternal
	}

	if !exists {
		return nil, status.Error(codes.InvalidArgument, "Account with this email does not exist")
	}

	student, err := s.repo.GetStudentByEmail(context.Background(), req.Email)
	if err != nil {
		s.logger.Error("Error while getting student by email ", err)
		return nil, grpcErrors.ErrInternal
	}

	resetToken, err := s.jwt.GenerateToken(req.Email, jwt.PasswordResetTokenDuration)
	if err != nil {
		s.logger.Error("Error while generating jwt reset token ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.UpdateStudentPasswordResetToken(ctx, req.Email, resetToken)
	if err != nil {
		s.logger.Error("Error while updating Student reset password token ", err)
		return nil, grpcErrors.ErrInternal
	}

	var e = common.ResetPasswordEvent{
		Name: common.ResetStudentPasswordEventName,
		Data: common.ResetPasswordEventPayload{
			Name:               student.Name,
			Email:              student.Email,
			ResetPasswordToken: resetToken,
			Subject:            common.ResetPasswordEmailSubject,
		},
	}

	b, _ := json.Marshal(e)

	err = s.rabbitMQPublisher.PublishWithContext(ctx, common.RABBITMQ_STUDENT_MAIL_ROUTING_KEY, b, common.RABBITMQ_CONTENT_TYPE_JSON)
	if err != nil {
		s.logger.Error("Error while publishing event ", err)
		return nil, status.Error(codes.Internal, "Error while sending reset password email. Please try again later")
	}

	return &pb.Empty{}, nil
}

func (s *StudentService) ResetStudentPassword(ctx context.Context, req *pb.ResetStudentPasswordRequest) (*pb.Empty, error) {
	resetToken, err := s.jwt.ValidateToken(req.ResetPasswordToken)
	if err != nil {
		if err == jwt.ErrExpiredToken || err == jwt.ErrInvalidToken {
			return nil, status.Error(codes.InvalidArgument, "Reset token has expired. Please generate a new reset token")
		}

		s.logger.Error("Error while validating jwt reset token ", err)
		return nil, grpcErrors.ErrInternal
	}

	email := resetToken.Email

	exists, err := s.repo.CheckIfStudentExists(ctx, email)
	if err != nil {
		s.logger.Error("Error while checking if Student exists ", err)
		return nil, grpcErrors.ErrInternal
	}

	if !exists {
		return nil, status.Error(codes.InvalidArgument, "Account with this email does not exist")
	}

	student, err := s.repo.GetStudentByEmail(ctx, email)
	if err != nil {
		s.logger.Error("Error while getting Student by email ", err)
		return nil, grpcErrors.ErrInternal
	}

	if student.ResetPasswordToken == "" {
		return nil, status.Error(codes.InvalidArgument, "No reset token found")
	}

	if req.ResetPasswordToken != student.ResetPasswordToken {
		return nil, status.Error(codes.InvalidArgument, "Invalid reset token")
	}

	hashedPassword, err := bcrypt.HashPassword(req.Password)
	if err != nil {
		s.logger.Error("Error while hashing password", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.UpdateStudentPassword(ctx, email, hashedPassword)
	if err != nil {
		s.logger.Error("Error while updating student password ", err)
		return nil, grpcErrors.ErrInternal
	}

	// Change the reset password token to empty string
	err = s.repo.UpdateStudentPasswordResetToken(context.Background(), student.Email, "")
	if err != nil {
		s.logger.Error("Error while updating student reset password token ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.Empty{}, nil
}

func (s *StudentService) UpdateStudent(ctx context.Context, req *pb.UpdateStudentRequest) (*pb.Empty, error) {
	studentId, err := s.ValidateStudentToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating student token ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.UpdateStudent(ctx, studentId, req.Name)
	if err != nil {
		s.logger.Error("Error while updating student ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.Empty{}, nil
}

func (s *StudentService) DeleteStudent(ctx context.Context, req *pb.DeleteStudentRequest) (*pb.Empty, error) {
	studentId, err := s.ValidateStudentToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating student token ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.DeleteStudentById(ctx, studentId)
	if err != nil {
		s.logger.Error("Error while deleting student ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.Empty{}, nil
}
