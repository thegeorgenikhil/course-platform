package service

import (
	"context"
	"errors"

	"github.com/thegeorgenikhil/course-platform/pkg/jwt"
)

var (
	ErrInvalidToken = errors.New("invalid token")
)

func (s *StudentService) ValidateStudentToken(ctx context.Context, token string) (uint64, error) {
	cl, err := s.jwt.ValidateToken(token)
	if err != nil {
		if err == jwt.ErrExpiredToken || err == jwt.ErrInvalidToken {
			if err == jwt.ErrExpiredToken || err == jwt.ErrInvalidToken {
				return 0, ErrInvalidToken
			}

			s.logger.Error("Error while validating jwt token ", err)
			return 0, err
		}
	}

	email := cl.Email

	exists, err := s.repo.CheckIfStudentExists(ctx, email)
	if err != nil {
		s.logger.Error("Error while checking if student exists ", err)
		return 0, err
	}

	if !exists {
		return 0, errors.New("student with this email does not exist")
	}

	student, err := s.repo.GetStudentByEmail(ctx, email)
	if err != nil {
		s.logger.Error("Error while getting student by email ", err)
		return 0, err
	}

	if !student.IsVerified {
		return 0, errors.New("student is not verified")
	}

	return student.Id, nil
}
