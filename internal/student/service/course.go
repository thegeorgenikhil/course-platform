package service

import (
	"context"

	"github.com/thegeorgenikhil/course-platform/internal/course/mongo/models"
	grpcErrors "github.com/thegeorgenikhil/course-platform/pkg/grpc_errors"
	coursePb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *StudentService) GetAllCourses(ctx context.Context, req *pb.Empty) (*pb.GetAllCoursesResponse, error) {
	res, err := s.courseClient.GetAllCourses(ctx)
	if err != nil {
		s.logger.Error("Error while getting all courses ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	var courseRes []*coursePb.Course
	for _, course := range res.Courses {
		if course.CourseStatus == string(models.CourseStatusLive) {
			courseRes = append(courseRes, course)
		}
	}

	return &pb.GetAllCoursesResponse{
		Courses: courseRes,
	}, nil
}

func (s *StudentService) GetCourseById(ctx context.Context, req *pb.GetCourseByIdRequest) (*pb.GetCourseByIdResponse, error) {
	res, err := s.courseClient.GetCourseById(ctx, req.CourseId)
	if err != nil {
		s.logger.Error("Error while getting course by id ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	if res.Course.CourseStatus != string(models.CourseStatusLive) {
		return nil, status.Error(codes.PermissionDenied, "Course is not live")
	}

	if req.Token != "" {
		studentId, err := s.ValidateStudentToken(ctx, req.Token)
		if err != nil {
			if err == ErrInvalidToken {
				return nil, grpcErrors.ErrInvalidToken
			}
			s.logger.Error("Error while validating student token ", err)
			return nil, grpcErrors.ErrInternal
		}

		checkEnrolledRes, err := s.courseClient.CheckIfStudentEnrolledRequest(ctx, studentId, req.CourseId)
		if err != nil {
			s.logger.Error("Error while checking if student is enrolled ", err)
			return nil, grpcErrors.GenerateFromError(err)
		}

		// If student is not enrolled, remove video links
		if !checkEnrolledRes.Enrolled {
			for index := range res.Course.Lessons {
				res.Course.Lessons[index].Video = nil
			}
		}

		res.Course.IsEnrolled = checkEnrolledRes.Enrolled
		res.Course.EnrolledOn = checkEnrolledRes.EnrolledOn
	} else {
		// For guest users, remove video links
		for index := range res.Course.Lessons {
			res.Course.Lessons[index].Video = nil
		}
	}

	return &pb.GetCourseByIdResponse{
		Course: res.Course,
	}, nil
}

func (s *StudentService) BuyCourse(ctx context.Context, req *pb.BuyCourseRequest) (*pb.BuyCourseResponse, error) {
	studentId, err := s.ValidateStudentToken(ctx, req.Token)
	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating student token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.CheckoutCourse(ctx, studentId, req.CourseId)
	if err != nil {
		s.logger.Error("Error while checking out course ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.BuyCourseResponse{
		SessionId: res.SessionId,
	}, nil
}

func (s *StudentService) GetEnrolledCourses(ctx context.Context, req *pb.GetEnrolledCoursesRequest) (*pb.GetEnrolledCoursesResponse, error) {
	studentId, err := s.ValidateStudentToken(ctx, req.Token)
	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating student token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.GetEnrolledCourses(ctx, studentId)
	if err != nil {
		s.logger.Error("Error while getting enrolled courses ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.GetEnrolledCoursesResponse{
		Courses: res.Courses,
	}, nil
}

func (s *StudentService) GetVideoInfo(ctx context.Context, req *pb.GetVideoInfoRequest) (*pb.GetVideoInfoResponse, error) {
	studentId, err := s.ValidateStudentToken(ctx, req.Token)
	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating student token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.GetVideoInfo(ctx, req.CourseId, req.LessonId, req.VideoId, studentId, false)
	if err != nil {
		s.logger.Error("Error while getting enrolled courses ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.GetVideoInfoResponse{
		PlaybackId:        res.PlaybackId,
		Token:             res.Token,
		CourseTitle:       res.CourseTitle,
		LessonTitle:       res.LessonTitle,
		LessonDescription: res.LessonDescription,
	}, nil
}
