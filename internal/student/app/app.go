package app

import (
	"context"
	"fmt"
	"net"

	"github.com/thegeorgenikhil/course-platform/cmd/student/config"
	"github.com/thegeorgenikhil/course-platform/common"
	"github.com/thegeorgenikhil/course-platform/internal/student/client/course"
	"github.com/thegeorgenikhil/course-platform/internal/student/repo"
	"github.com/thegeorgenikhil/course-platform/internal/student/server"
	"github.com/thegeorgenikhil/course-platform/internal/student/service"
	"github.com/thegeorgenikhil/course-platform/pkg/jwt"
	"github.com/thegeorgenikhil/course-platform/pkg/logger"
	"github.com/thegeorgenikhil/course-platform/pkg/migrations"
	"github.com/thegeorgenikhil/course-platform/pkg/postgres"
	"github.com/thegeorgenikhil/course-platform/pkg/rabbitmq"
	"github.com/thegeorgenikhil/course-platform/pkg/rabbitmq/publisher"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
	"google.golang.org/grpc"
)

type App struct {
	cfg *config.Config
}

func New(cfg *config.Config) *App {
	return &App{
		cfg: cfg,
	}
}

func (a *App) Run(ctx context.Context) error {

	l := logger.New(logger.LoggerConfig{
		Level: a.cfg.Log.Level,
	})

	l.Infoln("starting app -> name: ", a.cfg.App.Name, " version: ", a.cfg.App.Version)

	pg := postgres.NewPostgres(l, a.cfg.Postgres.ConnStr)

	// If any configurations need to be made
	// pg.Configure(postgres.ConnAttempts(20),postgres.ConnTimeout(time.Second * 5))

	// connect to db
	err := pg.Connect()
	if err != nil {
		l.Errorln("not able to connect to db: ", err)
		return err
	}
	defer pg.Close()

	db := pg.GetDB()

	migration := migrations.NewMigrate(l)
	err = migration.Migrate(a.cfg.Postgres.ConnStr)
	if err != nil {
		l.Errorln("migrations failed")
		return err
	}

	// gRPC Server
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", a.cfg.GRPC.Port))
	if err != nil {
		l.Errorln("not able to listen: ", err)
		return err
	}

	courseCl, err := course.NewCourseServiceClient(a.cfg, l)
	if err != nil {
		l.Errorln("not able to connect to course service: ", err)
		return err
	}

	jwt := jwt.NewJWTGenerator(a.cfg.JWT.JWTSecret)

	rabbitmqConn, err := rabbitmq.NewRabbitMQConn(a.cfg.RabbitMQ.ConnStr, l)
	if err != nil {
		l.Errorln("not able to connect to rabbitmq: ", err)
		return err
	}

	publisher, err := publisher.NewPublisher(l, common.RABBITMQ_MAILER_EXCHANGE, rabbitmqConn)
	if err != nil {
		l.Errorln("not able to create publisher: ", err)
		return err
	}

	err = publisher.DeclareExchange()
	if err != nil {
		l.Errorln("not able to declare exchange: ", err)
		return err
	}

	repo := repo.NewStudentRepo(l, db)
	service := service.NewStudentService(l, repo, jwt, publisher, courseCl)

	s := server.NewStudentService(service)

	grpc := grpc.NewServer()

	pb.RegisterStudentServiceServer(grpc, s)

	l.Infoln("starting gRPC Server at port: ", a.cfg.GRPC.Port)

	return grpc.Serve(lis)
}
