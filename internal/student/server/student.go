package server

import (
	"context"

	"github.com/thegeorgenikhil/course-platform/internal/student/service"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
)

type Server struct {
	service *service.StudentService
}

// The following line is for better development experience, so that all the unimplemented methods and their params can be seen in the same file (by hovering) instead of having to go to the app.go file.
var server *Server = nil
var _ pb.StudentServiceServer = server

func NewStudentService(studentService *service.StudentService) *Server {
	return &Server{
		service: studentService,
	}
}

func (s *Server) RegisterStudent(ctx context.Context, req *pb.RegisterStudentRequest) (*pb.Empty, error) {
	return s.service.RegisterStudent(ctx, req)
}

func (s *Server) ResendVerificationEmail(ctx context.Context, req *pb.ResendVerificationEmailRequest) (*pb.Empty, error) {
	return s.service.ResendVerificationEmail(ctx, req)
}

func (s *Server) VerifyStudentEmail(ctx context.Context, req *pb.VerifyStudentEmailRequest) (*pb.Empty, error) {
	return s.service.VerifyStudentEmail(ctx, req)
}

func (s *Server) LoginStudent(ctx context.Context, req *pb.LoginStudentRequest) (*pb.LoginStudentResponse, error) {
	return s.service.LoginStudent(ctx, req)
}

func (s *Server) GetStudentInfo(ctx context.Context, req *pb.GetStudentInfoRequest) (*pb.GetStudentInfoResponse, error) {
	return s.service.GetStudentInfo(ctx, req)
}

func (s *Server) SendPasswordResetToken(ctx context.Context, req *pb.SendPasswordResetTokenRequest) (*pb.Empty, error) {
	return s.service.SendPasswordResetToken(ctx, req)
}

func (s *Server) ResetStudentPassword(ctx context.Context, req *pb.ResetStudentPasswordRequest) (*pb.Empty, error) {
	return s.service.ResetStudentPassword(ctx, req)
}

func (s *Server) UpdateStudent(ctx context.Context, req *pb.UpdateStudentRequest) (*pb.Empty, error) {
	return s.service.UpdateStudent(ctx, req)
}

func (s *Server) DeleteStudent(ctx context.Context, req *pb.DeleteStudentRequest) (*pb.Empty, error) {
	return s.service.DeleteStudent(ctx, req)
}

func (s *Server) GetAllCourses(ctx context.Context, req *pb.Empty) (*pb.GetAllCoursesResponse, error) {
	return s.service.GetAllCourses(ctx, req)
}

func (s *Server) GetCourseById(ctx context.Context, req *pb.GetCourseByIdRequest) (*pb.GetCourseByIdResponse, error) {
	return s.service.GetCourseById(ctx, req)
}

func (s *Server) BuyCourse(ctx context.Context, req *pb.BuyCourseRequest) (*pb.BuyCourseResponse, error) {
	return s.service.BuyCourse(ctx, req)
}

func (s *Server) GetVideoInfo(ctx context.Context, req *pb.GetVideoInfoRequest) (*pb.GetVideoInfoResponse, error) {
	return s.service.GetVideoInfo(ctx, req)
}

func (s *Server) GetEnrolledCourses(ctx context.Context, req *pb.GetEnrolledCoursesRequest) (*pb.GetEnrolledCoursesResponse, error) {
	return s.service.GetEnrolledCourses(ctx, req)
}
