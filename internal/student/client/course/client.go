package course

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/student/config"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type CourseServiceClient struct {
	client pb.CourseServiceClient
	logger *logrus.Logger
}

func NewCourseServiceClient(c *config.Config, l *logrus.Logger) (*CourseServiceClient, error) {
	courseServiceURL := fmt.Sprintf("%s:%d", c.CourseService.Host, c.CourseService.Port)
	conn, err := grpc.Dial(courseServiceURL, grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		return nil, err
	}

	return &CourseServiceClient{
		client: pb.NewCourseServiceClient(conn),
		logger: l,
	}, nil
}

func (c *CourseServiceClient) GetAllCourses(ctx context.Context) (*pb.GetAllCoursesResponse, error) {
	req := &pb.Empty{}
	return c.client.GetAllCourses(ctx, req)
}

func (c *CourseServiceClient) GetCourseById(ctx context.Context, id string) (*pb.GetCourseByIdResponse, error) {
	req := &pb.GetCourseByIdRequest{
		CourseId: id,
	}
	return c.client.GetCourseById(ctx, req)
}

func (c *CourseServiceClient) CheckoutCourse(ctx context.Context, studentId uint64, courseId string) (*pb.BuyCourseResponse, error) {
	req := &pb.BuyCourseRequest{
		StudentId: studentId,
		CourseId:  courseId,
	}
	return c.client.BuyCourse(ctx, req)
}

func (c *CourseServiceClient) GetEnrolledCourses(ctx context.Context, studentId uint64) (*pb.GetEnrolledCoursesResponse, error) {
	req := &pb.GetEnrolledCoursesRequest{
		StudentId: studentId,
	}
	return c.client.GetEnrolledCourses(ctx, req)
}

func (c *CourseServiceClient) CheckIfStudentEnrolledRequest(ctx context.Context, studentId uint64, courseId string) (*pb.CheckIfStudentEnrolledResponse, error) {
	req := &pb.CheckIfStudentEnrolledRequest{
		StudentId: studentId,
		CourseId:  courseId,
	}
	return c.client.CheckIfStudentEnrolled(ctx, req)
}

func (c *CourseServiceClient) GetVideoInfo(ctx context.Context, courseId string, lessonId string, videoId string, studentId uint64, isAdmin bool) (*pb.GetVideoInfoResponse, error) {
	req := &pb.GetVideoInfoRequest{
		CourseId:  courseId,
		LessonId:  lessonId,
		VideoId:   videoId,
		StudentId: studentId,
		IsAdmin:   isAdmin,
	}
	return c.client.GetVideoInfo(ctx, req)
}
