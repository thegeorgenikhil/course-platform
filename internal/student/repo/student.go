package repo

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/student-gen"
)

type StudentRepo struct {
	logger *logrus.Logger
	db     *sql.DB
}

func NewStudentRepo(logger *logrus.Logger, db *sql.DB) *StudentRepo {
	return &StudentRepo{
		logger: logger,
		db:     db,
	}
}

func (r *StudentRepo) CreateStudent(
	ctx context.Context,
	name string,
	email string,
	password string,
	isVerified bool,
	verificationToken string,
	passwordResetToken string,
) error {
	sql := `INSERT INTO students (name, email, password, is_verified, verification_token, password_reset_token) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`

	var id int64
	err := r.db.QueryRowContext(ctx, sql, name, email, password, isVerified, verificationToken, passwordResetToken).Scan(&id)
	if err != nil {
		return err
	}

	r.logger.Infof("Created student with id %d", id)

	return nil
}

func (r *StudentRepo) GetStudentById(ctx context.Context, id uint64) (*pb.Student, error) {
	sql := `SELECT id, name, email, password, is_verified, verification_token, password_reset_token FROM students WHERE id = $1`

	var student pb.Student
	err := r.db.QueryRowContext(ctx, sql, id).Scan(&student.Id, &student.Name, &student.Email, &student.Password, &student.IsVerified, &student.VerificationToken, &student.ResetPasswordToken)
	if err != nil {
		return nil, err
	}

	return &student, nil
}

func (r *StudentRepo) CheckIfStudentExists(ctx context.Context, email string) (bool, error) {
	sql := `SELECT COUNT(*) FROM students WHERE email = $1`

	var count int
	err := r.db.QueryRowContext(ctx, sql, email).Scan(&count)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func (r *StudentRepo) GetStudentByEmail(ctx context.Context, email string) (*pb.Student, error) {
	sql := `SELECT id, name, email, password, is_verified, verification_token, password_reset_token FROM students WHERE email = $1`

	var student pb.Student
	err := r.db.QueryRowContext(ctx, sql, email).Scan(&student.Id, &student.Name, &student.Email, &student.Password, &student.IsVerified, &student.VerificationToken, &student.ResetPasswordToken)
	if err != nil {
		return nil, err
	}

	return &student, nil
}

func (r *StudentRepo) UpdateStudentVerificationToken(ctx context.Context, email string, verificationToken string) error {
	sql := `UPDATE students SET verification_token = $1 WHERE email = $2`

	_, err := r.db.ExecContext(ctx, sql, verificationToken, email)
	if err != nil {
		return err
	}

	return nil
}

func (r *StudentRepo) UpdateStudentVerificationStatus(ctx context.Context, email string) error {
	sql := `UPDATE students SET is_verified = TRUE WHERE email = $1`

	_, err := r.db.ExecContext(ctx, sql, email)
	if err != nil {
		return err
	}

	return nil
}

func (r *StudentRepo) UpdateStudentPasswordResetToken(ctx context.Context, email string, passwordResetToken string) error {
	sql := `UPDATE students SET password_reset_token = $1 WHERE email = $2`

	_, err := r.db.ExecContext(ctx, sql, passwordResetToken, email)
	if err != nil {
		return err
	}

	return nil
}

func (r *StudentRepo) UpdateStudentPassword(ctx context.Context, email string, newPassword string) error {
	sql := `UPDATE students SET password = $1 WHERE email = $2`

	_, err := r.db.ExecContext(ctx, sql, newPassword, email)
	if err != nil {
		return err
	}

	return nil
}

func (r *StudentRepo) UpdateStudent(ctx context.Context, id uint64, name string) error {
	sql := `UPDATE students SET name = $1 WHERE id = $2`

	_, err := r.db.ExecContext(ctx, sql, name, id)
	if err != nil {
		return err
	}

	return nil
}

func (r *StudentRepo) DeleteStudentById(ctx context.Context, id uint64) error {
	sql := `DELETE FROM students WHERE id = $1`

	_, err := r.db.ExecContext(ctx, sql, id)
	if err != nil {
		return err
	}

	return nil
}
