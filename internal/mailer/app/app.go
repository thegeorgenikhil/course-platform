package app

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/mailer/config"
	"github.com/thegeorgenikhil/course-platform/common"
	"github.com/thegeorgenikhil/course-platform/internal/mailer/mail"
	"github.com/thegeorgenikhil/course-platform/pkg/logger"
	"github.com/thegeorgenikhil/course-platform/pkg/rabbitmq"
	"github.com/thegeorgenikhil/course-platform/pkg/rabbitmq/consumer"
)

type App struct {
	cfg *config.Config
}

func New(cfg *config.Config) *App {
	return &App{
		cfg: cfg,
	}
}

func (a *App) RunWorker(ctx context.Context) error {

	l := logger.New(logger.LoggerConfig{
		Level: a.cfg.Log.Level,
	})

	l.Infoln("starting app -> name: ", a.cfg.App.Name, " version: ", a.cfg.App.Version)

	frontendUrl := os.Getenv("FRONTEND_URL")
	if frontendUrl == "" {
		frontendUrl = "http://127.0.0.1:5173"
	}

	adminFrontendUrl := os.Getenv("ADMIN_FRONTEND_URL")
	if adminFrontendUrl == "" {
		adminFrontendUrl = "http://localhost:5174"
	}

	mailer := mail.NewMailer(a.cfg.MailServer)

	rabbitmqConn, err := rabbitmq.NewRabbitMQConn(a.cfg.RabbitMQ.ConnStr, l)
	if err != nil {
		l.Errorln("not able to connect to rabbitmq: ", err)
		return err
	}

	consumer, err := consumer.NewConsumer(l, rabbitmqConn)
	if err != nil {
		l.Errorln("not able to create consumer: ", err)
		return err
	}

	err = consumer.Consume(
		common.RABBITMQ_MAILER_EXCHANGE,
		common.RABBITMQ_MAILER_QUEUE,
		[]string{
			common.RABBITMQ_INSTRUCTOR_MAIL_ROUTING_KEY,
			common.RABBITMQ_STUDENT_MAIL_ROUTING_KEY,
		},
	)
	if err != nil {
		l.Errorln("not able to consume: ", err)
		return err
	}

	msgs := consumer.GetDelivery()

	for {
		d := <-msgs

		var event common.Event
		err := json.Unmarshal(d.Body, &event)
		if err != nil {
			l.Errorln("not able to unmarshal event: ", err)
			continue
		}

		switch event.Name {
		case common.LoginInstructorEventName:
			err := sendLoginInstructorMail(adminFrontendUrl, l, mailer, d.Body)
			if err != nil {
				l.Errorln("not able to send login instructor mail: ", err)
				continue
			}

			d.Ack(false)

		case common.RegisterStudentEventName:
			err := sendStudentActivationMail(frontendUrl, l, mailer, d.Body)
			if err != nil {
				l.Errorln("not able to send student activation mail: ", err)
				continue
			}

			d.Ack(false)

		case common.ResendStudentVerificationEventName:
			err := sendStudentActivationMail(frontendUrl, l, mailer, d.Body)
			if err != nil {
				l.Errorln("not able to send student resend activation mail: ", err)
				continue
			}

			d.Ack(false)

		case common.ResetStudentPasswordEventName:
			err := sendStudentResetPasswordMail(frontendUrl, l, mailer, d.Body)
			if err != nil {
				l.Errorln("not able to send student reset password mail: ", err)
				continue
			}

			d.Ack(false)

		default:
			l.Errorln("unknown event: ", event.Name)
		}
	}
}

func sendLoginInstructorMail(frontendHost string, l *logrus.Logger, mailer *mail.Mailer, data []byte) error {
	var loginInstructorEvent common.LoginInstructorEvent
	err := json.Unmarshal(data, &loginInstructorEvent)
	if err != nil {
		return err
	}

	mailInfo := map[string]string{
		"Link": fmt.Sprintf("%s/api/auth/login?token=%s", frontendHost, loginInstructorEvent.Data.AuthToken),
	}

	content, err := mail.ParseTemplate(l, mail.LoginInstructorTemplateFile, mailInfo)
	if err != nil {
		return err
	}

	mail := mail.NewMail(loginInstructorEvent.Data.Email, content, mail.LoginInstructorSubject)

	ok, err := mailer.SendMail(mail)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New("mail not sent to " + mail.To)
	}

	l.Infoln("mail sent successfully to ", mail.To)

	return nil
}

func sendStudentActivationMail(frontendHost string, l *logrus.Logger, mailer *mail.Mailer, data []byte) error {
	var registerStudentEvent common.RegisterEvent
	err := json.Unmarshal(data, &registerStudentEvent)
	if err != nil {
		return err
	}

	mailInfo := map[string]string{
		"Name": registerStudentEvent.Data.Name,
		"Link": fmt.Sprintf("%s/api/auth/verify/?token=%s", frontendHost, registerStudentEvent.Data.VerificationToken),
	}

	content, err := mail.ParseTemplate(l, mail.ActivateStudentAccountTemplateFile, mailInfo)
	if err != nil {
		return err
	}

	mail := mail.NewMail(registerStudentEvent.Data.Email, content, mail.ActivateAccountSubject)

	ok, err := mailer.SendMail(mail)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New("mail not sent to " + mail.To)
	}

	l.Infoln("mail sent successfully to ", mail.To)

	return nil
}

func sendStudentResetPasswordMail(frontendHost string, l *logrus.Logger, mailer *mail.Mailer, data []byte) error {
	var resetPasswordEvent common.ResetPasswordEvent
	err := json.Unmarshal(data, &resetPasswordEvent)
	if err != nil {
		return err
	}

	mailInfo := map[string]string{
		"Name": resetPasswordEvent.Data.Name,
		"Link": fmt.Sprintf("%s/auth/reset-password?token=%s", frontendHost, resetPasswordEvent.Data.ResetPasswordToken),
	}

	content, err := mail.ParseTemplate(l, mail.ResetStudentPasswordTemplateFile, mailInfo)
	if err != nil {
		return err
	}

	mail := mail.NewMail(resetPasswordEvent.Data.Email, content, mail.ResetPasswordSubject)

	ok, err := mailer.SendMail(mail)
	if err != nil {
		return err
	}

	if !ok {
		return errors.New("mail not sent to " + mail.To)
	}

	l.Infoln("mail sent successfully to ", mail.To)

	return nil
}
