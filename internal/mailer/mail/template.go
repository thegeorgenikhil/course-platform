package mail

import (
	"bytes"
	"html/template"
	"path/filepath"

	"github.com/sirupsen/logrus"
)

const (
	LoginInstructorTemplateFile = "instructorLogin.html.tmpl"

	ActivateStudentAccountTemplateFile = "activateStudent.html.tmpl"
	ResetStudentPasswordTemplateFile   = "resetStudentPassword.html.tmpl"

	LoginInstructorSubject = "Login to your account | Course Platform"

	ActivateAccountSubject = "Activate your account | Course Platform"
	ResetPasswordSubject   = "Reset your password | Course Platform"
)

func ParseTemplate(l *logrus.Logger, fileName string, info any) (string, error) {
	t := template.New(fileName)

	templateFilePath := filepath.Join("templates", fileName)

	var err error
	t, err = t.ParseFiles(templateFilePath)
	if err != nil {
		l.Println("Error while parsing file:" + err.Error())
		return "", err
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, info); err != nil {
		l.Println("Error while executing template execution:" + err.Error())
		return "", err
	}

	result := tpl.String()

	return result, nil
}
