package server

import (
	"context"

	"github.com/thegeorgenikhil/course-platform/internal/instructor/service"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
)

type Server struct {
	service *service.InstructorService
}

// The following line is for better development experience, so that all the unimplemented methods and their params can be seen in the same file (by hovering) instead of having to go to the app.go file.
var server *Server = nil
var _ pb.InstructorServiceServer = server

func NewInstructorServer(instructorService *service.InstructorService) *Server {
	return &Server{
		service: instructorService,
	}
}

func (s *Server) SendLoginInstructorMail(ctx context.Context, req *pb.SendLoginInstructorMailRequest) (*pb.Empty, error) {
	return s.service.SendLoginInstructorMail(ctx, req)
}

func (s *Server) AuthenticateInstructor(ctx context.Context, req *pb.AuthenticateInstructorRequest) (*pb.AuthenticateInstructorResponse, error) {
	return s.service.AuthenticateInstructor(ctx, req)
}

func (s *Server) GetInstructorInfo(ctx context.Context, req *pb.Empty) (*pb.GetInstructorInfoResponse, error) {
	return s.service.GetInstructorInfo(ctx, req)
}

func (s *Server) UpdateInstructor(ctx context.Context, req *pb.UpdateInstructorRequest) (*pb.Empty, error) {
	return s.service.UpdateInstructor(ctx, req)
}

func (s *Server) CreateCourse(ctx context.Context, req *pb.CreateCourseRequest) (*pb.CreateCourseResponse, error) {
	return s.service.CreateCourse(ctx, req)
}

func (s *Server) GetAllCourses(ctx context.Context, req *pb.GetAllCoursesRequest) (*pb.GetAllCoursesResponse, error) {
	return s.service.GetAllCourses(ctx, req)
}

func (s *Server) GetCourseById(ctx context.Context, req *pb.GetCourseByIdRequest) (*pb.GetCourseByIdResponse, error) {
	return s.service.GetCourseById(ctx, req)
}

func (s *Server) UpdateCourse(ctx context.Context, req *pb.UpdateCourseRequest) (*pb.Empty, error) {
	return s.service.UpdateCourse(ctx, req)
}

func (s *Server) AddLessonToCourse(ctx context.Context, req *pb.AddLessonToCourseRequest) (*pb.AddLessonToCourseResponse, error) {
	return s.service.AddLessonToCourse(ctx, req)
}

func (s *Server) AddVideoToLesson(ctx context.Context, req *pb.AddVideoToLessonRequest) (*pb.AddVideoToLessonResponse, error) {
	return s.service.AddVideoToCourse(ctx, req)
}

func (s *Server) GetVideoInfo(ctx context.Context, req *pb.GetVideoInfoRequest) (*pb.GetVideoInfoResponse, error) {
	return s.service.GetVideoInfo(ctx, req)
}

func (s *Server) PublishCourse(ctx context.Context, req *pb.PublishCourseRequest) (*pb.Empty, error) {
	return s.service.PublishCourse(ctx, req)
}
