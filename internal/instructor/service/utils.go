package service

import (
	"context"
	"errors"

	"github.com/thegeorgenikhil/course-platform/pkg/jwt"
)

var (
	ErrInvalidToken = errors.New("invalid token")
)

func (s *InstructorService) CreateInstructorIfNotExists(ctx context.Context, email string, name string) error {
	count, err := s.repo.GetInstructorCount(ctx)
	if err != nil {
		s.logger.Error("Error while getting instructor count ", err)
		return err
	}

	if count == 1 {
		s.logger.Info("Skipping instructor creation as there is already one instructor")
		return nil
	}

	exists, err := s.repo.CheckIfInstructorExists(ctx, email)
	if err != nil {
		s.logger.Error("Error while checking if instructor exists ", err)
		return err
	}

	if !exists {
		_, err = s.repo.CreateInstructor(ctx, name, email)
		if err != nil {
			s.logger.Error("Error while creating instructor ", err)
			return err
		}
		s.logger.Info("Created instructor with email: ", email)
	}

	return nil
}

func (s *InstructorService) ValidateInstructorToken(ctx context.Context, token string) (uint64, error) {
	cl, err := s.jwt.ValidateToken(token)
	if err != nil {
		if err == jwt.ErrExpiredToken || err == jwt.ErrInvalidToken {
			return 0, ErrInvalidToken
		}

		s.logger.Error("Error while validating jwt token ", err)
		return 0, err
	}

	email := cl.Email

	exists, err := s.repo.CheckIfInstructorExists(ctx, email)
	if err != nil {
		s.logger.Error("Error while checking if instructor exists ", err)
		return 0, err
	}

	if !exists {
		return 0, errors.New("instructor with this email does not exist")
	}

	instructor, err := s.repo.GetInstructorByEmail(ctx, email)
	if err != nil {
		s.logger.Error("Error while getting instructor by email ", err)
		return 0, err
	}

	return instructor.Id, nil
}
