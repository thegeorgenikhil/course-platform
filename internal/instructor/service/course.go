package service

import (
	"context"

	grpcErrors "github.com/thegeorgenikhil/course-platform/pkg/grpc_errors"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
)

func (s *InstructorService) CreateCourse(ctx context.Context, req *pb.CreateCourseRequest) (*pb.CreateCourseResponse, error) {
	_, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.CreateCourse(
		ctx,
		req.Title,
		req.Description,
		req.Category,
		req.Price,
	)

	if err != nil {
		s.logger.Error("Error while creating course ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.CreateCourseResponse{
		CourseId: res.CourseId,
	}, nil
}

func (s *InstructorService) GetAllCourses(ctx context.Context, req *pb.GetAllCoursesRequest) (*pb.GetAllCoursesResponse, error) {
	_, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.GetAllCourses(ctx)

	if err != nil {
		s.logger.Error("Error while fetching all courses ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.GetAllCoursesResponse{
		Courses: res.Courses,
	}, nil
}

func (s *InstructorService) GetCourseById(ctx context.Context, req *pb.GetCourseByIdRequest) (*pb.GetCourseByIdResponse, error) {
	_, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.GetCourseById(ctx, req.CourseId)

	if err != nil {
		s.logger.Error("Error while getting course info ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.GetCourseByIdResponse{
		Course: res.Course,
	}, nil
}

func (s *InstructorService) UpdateCourse(ctx context.Context, req *pb.UpdateCourseRequest) (*pb.Empty, error) {
	_, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	_, err = s.courseClient.UpdateCourse(ctx, req.CourseId, req.Title, req.Description, req.Category, req.Price)

	if err != nil {
		s.logger.Error("Error while getting course updating ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.Empty{}, nil
}

func (s *InstructorService) AddLessonToCourse(ctx context.Context, req *pb.AddLessonToCourseRequest) (*pb.AddLessonToCourseResponse, error) {
	_, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.AddLessonToCourse(
		ctx,
		req.CourseId,
		req.Name,
		req.Description,
	)

	if err != nil {
		s.logger.Error("Error while getting course info ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.AddLessonToCourseResponse{
		Lesson: res.Lesson,
	}, nil
}

func (s *InstructorService) AddVideoToCourse(ctx context.Context, req *pb.AddVideoToLessonRequest) (*pb.AddVideoToLessonResponse, error) {
	_, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.AddVideoToLesson(
		ctx,
		req.CourseId,
		req.LessonId,
	)

	if err != nil {
		s.logger.Error("Error while getting video upload url ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.AddVideoToLessonResponse{
		PreSignedUploadUrl: res.PreSignedUploadUrl,
	}, nil
}

func (s *InstructorService) PublishCourse(ctx context.Context, req *pb.PublishCourseRequest) (*pb.Empty, error) {
	_, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	_, err = s.courseClient.PublishCourse(ctx, req.CourseId)

	if err != nil {
		s.logger.Error("Error while publishing course ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.Empty{}, nil
}

func (s *InstructorService) GetVideoInfo(ctx context.Context, req *pb.GetVideoInfoRequest) (*pb.GetVideoInfoResponse, error) {
	_, err := s.ValidateInstructorToken(ctx, req.Token)
	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating student token ", err)
		return nil, grpcErrors.ErrInternal
	}

	res, err := s.courseClient.GetVideoInfo(ctx, req.CourseId, req.LessonId, req.VideoId, 0, true)
	if err != nil {
		s.logger.Error("Error while getting enrolled courses ", err)
		return nil, grpcErrors.GenerateFromError(err)
	}

	return &pb.GetVideoInfoResponse{
		PlaybackId:        res.PlaybackId,
		Token:             res.Token,
		CourseTitle:       res.CourseTitle,
		LessonTitle:       res.LessonTitle,
		LessonDescription: res.LessonDescription,
	}, nil
}
