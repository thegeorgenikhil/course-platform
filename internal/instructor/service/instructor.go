package service

import (
	"context"
	"database/sql"
	"encoding/json"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/common"
	"github.com/thegeorgenikhil/course-platform/internal/instructor/client/course"
	"github.com/thegeorgenikhil/course-platform/internal/instructor/repo"
	grpcErrors "github.com/thegeorgenikhil/course-platform/pkg/grpc_errors"
	"github.com/thegeorgenikhil/course-platform/pkg/jwt"
	"github.com/thegeorgenikhil/course-platform/pkg/rabbitmq/publisher"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type InstructorService struct {
	logger            *logrus.Logger
	repo              *repo.InstructorRepo
	jwt               *jwt.JWTGenerator
	rabbitMQPublisher *publisher.Publisher
	courseClient      *course.CourseServiceClient
}

func NewInstructorService(logger *logrus.Logger, instructorRepo *repo.InstructorRepo, jwt *jwt.JWTGenerator, publisher *publisher.Publisher, courseClient *course.CourseServiceClient) *InstructorService {
	return &InstructorService{
		logger:            logger,
		repo:              instructorRepo,
		jwt:               jwt,
		rabbitMQPublisher: publisher,
		courseClient:      courseClient,
	}
}

func (s *InstructorService) SendLoginInstructorMail(ctx context.Context, req *pb.SendLoginInstructorMailRequest) (*pb.Empty, error) {
	exists, err := s.repo.CheckIfInstructorExists(ctx, req.Email)
	if err != nil {
		s.logger.Error("Error while checking if instructor exists ", err)
		return nil, grpcErrors.ErrInternal
	}
	s.logger.Info("Instructor exists: ", exists)

	if !exists {
		return nil, status.Error(codes.InvalidArgument, "Instructor with this email does not exist")
	}

	token, err := s.jwt.GenerateToken(req.Email, jwt.AccessTokenDuration)
	if err != nil {
		s.logger.Error("Error while generating jwt token ", err)
		return nil, grpcErrors.ErrInternal
	}

	// send a login mail to the instructor
	var e = common.Event{
		Name: common.LoginInstructorEventName,
		Data: common.LoginInstructorEventPayload{
			Email:     req.Email,
			AuthToken: token,
			Subject:   common.LoginInstructorSubject,
		},
	}

	b, _ := json.Marshal(e)

	err = s.rabbitMQPublisher.PublishWithContext(ctx, common.RABBITMQ_INSTRUCTOR_MAIL_ROUTING_KEY, b, common.RABBITMQ_CONTENT_TYPE_JSON)
	if err != nil {
		s.logger.Error("Error while publishing event ", err)
		return nil, status.Error(codes.Internal, "Error while sending verification email. Please try re-sending the verification email")
	}

	return &pb.Empty{}, nil
}

func (s *InstructorService) AuthenticateInstructor(ctx context.Context, req *pb.AuthenticateInstructorRequest) (*pb.AuthenticateInstructorResponse, error) {
	id, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	instructor, err := s.repo.GetInstructorById(ctx, id)
	if err != nil {
		s.logger.Error("Error while getting instructor by id ", err)
		return nil, grpcErrors.ErrInternal
	}

	token, err := s.jwt.GenerateToken(instructor.Email, jwt.AccessTokenDuration)
	if err != nil {
		s.logger.Error("Error while generating jwt token ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.AuthenticateInstructorResponse{
		Token: token,
	}, nil
}

func (s *InstructorService) GetInstructorInfo(ctx context.Context, req *pb.Empty) (*pb.GetInstructorInfoResponse, error) {
	instructor, err := s.repo.GetInstructorById(ctx, 1)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, status.Error(codes.InvalidArgument, "Instructor with this email id does not exists")
		}

		s.logger.Error("Error while getting instructor by id ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.GetInstructorInfoResponse{
		Instructor: &pb.Instructor{
			Name: instructor.Name,
			Bio:  instructor.Bio,
		},
	}, nil
}

func (s *InstructorService) UpdateInstructor(ctx context.Context, req *pb.UpdateInstructorRequest) (*pb.Empty, error) {
	id, err := s.ValidateInstructorToken(ctx, req.Token)

	if err != nil {
		if err == ErrInvalidToken {
			return nil, grpcErrors.ErrInvalidToken
		}
		s.logger.Error("Error while validating instructor token ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.UpdateInstructor(ctx, id, req.Name, req.Bio)
	if err != nil {
		s.logger.Error("Error while updating instructor ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.Empty{}, nil
}
