package repo

import (
	"context"
	"database/sql"

	"github.com/sirupsen/logrus"
	pb "github.com/thegeorgenikhil/course-platform/proto/instructor-gen"
)

type InstructorRepo struct {
	logger *logrus.Logger
	db     *sql.DB
}

func NewInstructorRepo(logger *logrus.Logger, db *sql.DB) *InstructorRepo {
	return &InstructorRepo{
		logger: logger,
		db:     db,
	}
}

func (r *InstructorRepo) CreateInstructor(ctx context.Context, name string, email string) (uint64, error) {
	sql := `INSERT INTO instructors (name, email,bio) VALUES ($1, $2, $3) RETURNING id`

	var id uint64
	err := r.db.QueryRowContext(ctx, sql, name, email, "").Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r *InstructorRepo) GetInstructorCount(ctx context.Context) (uint64, error) {
	sql := `SELECT COUNT(*) FROM instructors`

	var count uint64
	err := r.db.QueryRowContext(ctx, sql).Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func (r *InstructorRepo) CheckIfInstructorExists(ctx context.Context, email string) (bool, error) {
	sql := `SELECT COUNT(*) FROM instructors WHERE email = $1`

	var count int
	err := r.db.QueryRowContext(ctx, sql, email).Scan(&count)
	if err != nil {
		return false, err
	}

	return count > 0, nil
}

func (r *InstructorRepo) GetInstructorById(ctx context.Context, id uint64) (*pb.Instructor, error) {
	sql := `SELECT id, name, email, bio FROM instructors WHERE id = $1`

	var instructor pb.Instructor
	err := r.db.QueryRowContext(ctx, sql, id).Scan(&instructor.Id, &instructor.Name, &instructor.Email, &instructor.Bio)
	if err != nil {
		return nil, err
	}

	return &instructor, nil
}

func (r *InstructorRepo) GetInstructorByEmail(ctx context.Context, email string) (*pb.Instructor, error) {
	sql := `SELECT id, name, email, bio FROM instructors WHERE email = $1`

	var instructor pb.Instructor
	err := r.db.QueryRowContext(ctx, sql, email).Scan(&instructor.Id, &instructor.Name, &instructor.Email, &instructor.Bio)
	if err != nil {
		return nil, err
	}

	return &instructor, nil
}

func (r *InstructorRepo) UpdateInstructor(ctx context.Context, id uint64, name string, bio string) error {
	sql := `UPDATE instructors SET name = $1, bio = $2 WHERE id = $3`

	_, err := r.db.ExecContext(ctx, sql, name, bio, id)
	if err != nil {
		return err
	}

	return nil
}
