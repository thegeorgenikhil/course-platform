package course

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/cmd/instructor/config"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type CourseServiceClient struct {
	client pb.CourseServiceClient
	logger *logrus.Logger
}

func NewCourseServiceClient(c *config.Config, l *logrus.Logger) (*CourseServiceClient, error) {
	courseServiceURL := fmt.Sprintf("%s:%d", c.CourseService.Host, c.CourseService.Port)
	conn, err := grpc.Dial(courseServiceURL, grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		return nil, err
	}

	return &CourseServiceClient{
		client: pb.NewCourseServiceClient(conn),
		logger: l,
	}, nil
}

func (c *CourseServiceClient) CreateCourse(ctx context.Context, title string, description string, category string, price uint64) (*pb.CreateCourseResponse, error) {
	req := &pb.CreateCourseRequest{
		Title:       title,
		Description: description,
		Category:    category,
		Price:       price,
	}
	return c.client.CreateCourse(ctx, req)
}

func (c *CourseServiceClient) GetAllCourses(ctx context.Context) (*pb.GetAllCoursesResponse, error) {
	req := &pb.Empty{}
	return c.client.GetAllCourses(ctx, req)
}

func (c *CourseServiceClient) GetCourseById(ctx context.Context, id string) (*pb.GetCourseByIdResponse, error) {
	req := &pb.GetCourseByIdRequest{
		CourseId: id,
	}
	return c.client.GetCourseById(ctx, req)
}

func (c *CourseServiceClient) UpdateCourse(ctx context.Context, courseId string, title string, description string, category string, price uint64) (*pb.Empty, error) {
	req := &pb.UpdateCourseRequest{
		CourseId:    courseId,
		Title:       title,
		Description: description,
		Category:    category,
		Price:       price,
	}
	return c.client.UpdateCourse(ctx, req)
}

func (c *CourseServiceClient) AddLessonToCourse(ctx context.Context, courseId string, name string, description string) (*pb.AddLessonToCourseResponse, error) {
	req := &pb.AddLessonToCourseRequest{
		CourseId:    courseId,
		Name:        name,
		Description: description,
	}
	return c.client.AddLessonToCourse(ctx, req)
}

func (c *CourseServiceClient) AddVideoToLesson(ctx context.Context, courseId string, lessonId string) (*pb.AddVideoToLessonResponse, error) {
	req := &pb.AddVideoToLessonRequest{
		CourseId: courseId,
		LessonId: lessonId,
	}
	return c.client.AddVideoToLesson(ctx, req)
}

func (c *CourseServiceClient) PublishCourse(ctx context.Context, courseId string) (*pb.Empty, error) {
	req := &pb.PublishCourseRequest{
		CourseId: courseId,
	}
	return c.client.PublishCourse(ctx, req)
}

func (c *CourseServiceClient) GetVideoInfo(ctx context.Context, courseId string, lessonId string, videoId string, studentId uint64, isAdmin bool) (*pb.GetVideoInfoResponse, error) {
	req := &pb.GetVideoInfoRequest{
		CourseId:  courseId,
		LessonId:  lessonId,
		VideoId:   videoId,
		StudentId: studentId,
		IsAdmin:   isAdmin,
	}
	return c.client.GetVideoInfo(ctx, req)
}
