package app

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"

	"github.com/thegeorgenikhil/course-platform/cmd/course/config"
	"github.com/thegeorgenikhil/course-platform/internal/course/mongo"
	"github.com/thegeorgenikhil/course-platform/internal/course/repo"
	"github.com/thegeorgenikhil/course-platform/internal/course/server"
	"github.com/thegeorgenikhil/course-platform/internal/course/service"
	"github.com/thegeorgenikhil/course-platform/pkg/logger"
	"github.com/thegeorgenikhil/course-platform/pkg/mux"
	"github.com/thegeorgenikhil/course-platform/pkg/stripe"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"google.golang.org/grpc"
)

type App struct {
	cfg *config.Config
}

func New(cfg *config.Config) *App {
	return &App{
		cfg: cfg,
	}
}

func (a *App) Run(ctx context.Context) error {

	l := logger.New(logger.LoggerConfig{
		Level: a.cfg.Log.Level,
	})

	l.Infoln("starting app -> name: ", a.cfg.App.Name, " version: ", a.cfg.App.Version)

	mgo, err := mongo.NewMongo(ctx, a.cfg.MongoDB.ConnStr, a.cfg.MongoDB.DBName)
	if err != nil {
		l.Error("not able to connect to mongo: ", err)
		return err
	}

	defer mgo.Close(ctx)

	db := mgo.GetDB()

	// gRPC Server
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", a.cfg.GRPC.Port))
	if err != nil {
		l.Errorln("not able to listen: ", err)
		return err
	}

	stripeSecretKey := os.Getenv("STRIPE_SECRET_KEY")
	if stripeSecretKey == "" {
		l.Errorln("stripe secret key not found")
		return errors.New("stripe secret key not found")
	}

	stripeRedirectURL := os.Getenv("STRIPE_REDIRECT_URL")
	if stripeRedirectURL == "" {
		stripeRedirectURL = "http://127.0.0.1:5173"
	}

	stripeCl := stripe.NewStripeClient(stripeRedirectURL, stripeSecretKey)

	muxCl, err := mux.NewMuxClient(
		os.Getenv("MUX_TOKEN_ID"),
		os.Getenv("MUX_TOKEN_SECRET"),
		os.Getenv("MUX_SIGNING_KEY_ID"),
		os.Getenv("MUX_SIGNING_KEY"),
	)

	if err != nil {
		l.Errorln("not able to create MUX client: ", err)
		return err
	}

	repo := repo.NewCourseRepo(l, db)
	service := service.NewCourseService(l, repo, muxCl, stripeCl)

	s := server.NewCourseServer(service)

	grpc := grpc.NewServer()

	pb.RegisterCourseServiceServer(grpc, s)

	l.Infoln("starting gRPC Server at port: ", a.cfg.GRPC.Port)

	return grpc.Serve(lis)
}
