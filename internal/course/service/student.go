package service

import (
	"context"
	"fmt"

	"github.com/thegeorgenikhil/course-platform/internal/course/mongo"
	"github.com/thegeorgenikhil/course-platform/internal/course/mongo/models"
	grpcErrors "github.com/thegeorgenikhil/course-platform/pkg/grpc_errors"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *CourseService) BuyCourse(ctx context.Context, req *pb.BuyCourseRequest) (*pb.BuyCourseResponse, error) {
	courseId, err := primitive.ObjectIDFromHex(req.CourseId)
	if err != nil {
		s.logger.Error("Error while converting course id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	course, err := s.repo.GetCourseById(ctx, courseId)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, status.Error(codes.InvalidArgument, "Course not found")
		}

		s.logger.Error("Error while getting course by id ", err)
		return nil, grpcErrors.ErrInternal
	}

	if course.CourseStatus != models.CourseStatusLive {
		return nil, status.Error(codes.InvalidArgument, "Course is not live")
	}

	courseCheckoutMetadata := map[string]string{
		"course_id":  course.ID.Hex(),
		"student_id": fmt.Sprintf("%d", req.StudentId),
	}

	successURL := fmt.Sprintf("%s/payment/success?course=%s", s.stripe.GetFrontendURL(), course.ID.Hex())
	cancelURL := fmt.Sprintf("%s/payment/failure?course=%s", s.stripe.GetFrontendURL(), course.ID.Hex())

	checkoutSession, err := s.stripe.CreateCheckoutSession(
		course.Title,
		int64(course.Price),
		courseCheckoutMetadata,
		successURL,
		cancelURL,
	)
	if err != nil {
		s.logger.Error("Error while creating checkout session ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.BuyCourseResponse{
		SessionId: checkoutSession.ID,
	}, nil
}

func (s *CourseService) GetEnrolledCourses(ctx context.Context, req *pb.GetEnrolledCoursesRequest) (*pb.GetEnrolledCoursesResponse, error) {
	studentId := req.StudentId

	courses, err := s.repo.GetEnrolledCourses(ctx, studentId)
	if err != nil {
		s.logger.Error("Error while getting enrolled courses ", err)
		return nil, grpcErrors.ErrInternal
	}

	var enrolledCourses []*pb.Course
	for _, course := range courses {
		enrolledCourses = append(enrolledCourses, &pb.Course{
			Id:          course.ID.Hex(),
			Title:       course.Title,
			Description: course.Description,
			Category:    course.Category,
			CreatedAt:   course.CreatedAt.String(),
			UpdatedAt:   course.UpdatedAt.String(),
		})
	}

	return &pb.GetEnrolledCoursesResponse{
		Courses: enrolledCourses,
	}, nil
}
