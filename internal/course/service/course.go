package service

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/internal/course/mongo/models"
	"github.com/thegeorgenikhil/course-platform/internal/course/repo"
	grpcErrors "github.com/thegeorgenikhil/course-platform/pkg/grpc_errors"
	"github.com/thegeorgenikhil/course-platform/pkg/mux"
	"github.com/thegeorgenikhil/course-platform/pkg/stripe"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CourseService struct {
	logger    *logrus.Logger
	repo      *repo.CourseRepo
	muxClient *mux.MuxClient
	stripe    *stripe.StripeClient
}

func NewCourseService(logger *logrus.Logger, courseRepo *repo.CourseRepo, muxClient *mux.MuxClient, stripeClient *stripe.StripeClient) *CourseService {
	return &CourseService{
		logger:    logger,
		repo:      courseRepo,
		muxClient: muxClient,
		stripe:    stripeClient,
	}
}

func (s *CourseService) CreateCourse(ctx context.Context, req *pb.CreateCourseRequest) (*pb.CreateCourseResponse, error) {
	var c = models.Course{
		Title:                 req.Title,
		Description:           req.Description,
		Category:              req.Category,
		CourseStatus:          models.CourseStatusDraft,
		TotalStudentsEnrolled: 0,
		TotalLessons:          0,
		Duration:              0,
		Lessons:               []models.Lesson{},
		Price:                 req.Price,
		CreatedAt:             time.Now().UTC(),
		UpdatedAt:             time.Now().UTC(),
	}

	course, err := s.repo.CreateNewCourse(ctx, c)
	if err != nil {
		s.logger.Error("Error while creating new course ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.CreateCourseResponse{
		CourseId: string(course.Hex()),
	}, nil
}

func (s *CourseService) GetAllCourses(ctx context.Context, req *pb.Empty) (*pb.GetAllCoursesResponse, error) {
	courses, err := s.repo.GetAllCourses(ctx)
	if err != nil {
		s.logger.Error("Error while getting all courses ", err)
		return nil, grpcErrors.ErrInternal
	}

	var resCourses []*pb.Course
	for _, c := range courses {
		resCourses = append(resCourses, &pb.Course{
			Id:                    string(c.ID.Hex()),
			Title:                 c.Title,
			Description:           c.Description,
			Category:              c.Category,
			CourseStatus:          string(c.CourseStatus),
			TotalStudentsEnrolled: c.TotalStudentsEnrolled,
			TotalLessons:          c.TotalLessons,
			TotalDuration:         c.Duration,
			Price:                 c.Price,
			CreatedAt:             c.CreatedAt.String(),
			UpdatedAt:             c.UpdatedAt.String(),
		})
	}

	return &pb.GetAllCoursesResponse{
		Courses: resCourses,
	}, nil
}

func (s *CourseService) GetCourseById(ctx context.Context, req *pb.GetCourseByIdRequest) (*pb.GetCourseByIdResponse, error) {
	objId, err := primitive.ObjectIDFromHex(req.CourseId)
	if err != nil {
		s.logger.Error("Error while converting course id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	course, err := s.repo.GetCourseById(ctx, objId)
	if err != nil {
		s.logger.Error("Error while getting course info ", err)
		return nil, grpcErrors.ErrInternal
	}

	var courseLesson []*pb.Lesson
	for _, l := range course.Lessons {
		lesson := &pb.Lesson{
			Id:          l.ID.Hex(),
			Name:        l.Name,
			Description: l.Description,
			Video:       nil,
			Duration:    l.Duration,
			CreatedAt:   l.CreatedAt.String(),
			UpdatedAt:   l.UpdatedAt.String(),
		}

		if !l.Video.ID.IsZero() {
			lesson.Video = &pb.Video{
				Id:        l.Video.ID.Hex(),
				Status:    string(l.Video.Status),
				Duration:  l.Video.Duration,
				CreatedAt: l.Video.CreatedAt.String(),
				UpdatedAt: l.Video.UpdatedAt.String(),
			}
		}

		courseLesson = append(courseLesson, lesson)
	}

	courseRes := &pb.Course{
		Id:                    course.ID.Hex(),
		Title:                 course.Title,
		Description:           course.Description,
		Category:              course.Category,
		CourseStatus:          string(course.CourseStatus),
		TotalStudentsEnrolled: course.TotalStudentsEnrolled,
		TotalLessons:          course.TotalLessons,
		Price:                 course.Price,
		Lessons:               courseLesson,
		TotalDuration:         course.Duration,
		CreatedAt:             course.CreatedAt.String(),
		UpdatedAt:             course.UpdatedAt.String(),
	}

	return &pb.GetCourseByIdResponse{
		Course: courseRes,
	}, nil
}

func (s *CourseService) UpdateCourse(ctx context.Context, req *pb.UpdateCourseRequest) (*pb.Empty, error) {
	objId, err := primitive.ObjectIDFromHex(req.CourseId)
	if err != nil {
		s.logger.Error("Error while converting course id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	_, err = s.repo.GetCourseById(ctx, objId)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, status.Error(codes.InvalidArgument, "Course not found")
		}
		s.logger.Error("Error while getting course info ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.UpdateCourse(ctx, objId, req.Title, req.Description, req.Category, req.Price)
	if err != nil {
		s.logger.Error("Error while updating course ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.Empty{}, nil
}

func (s *CourseService) AddLessonToCourse(ctx context.Context, req *pb.AddLessonToCourseRequest) (*pb.AddLessonToCourseResponse, error) {
	courseObjId, err := primitive.ObjectIDFromHex(req.CourseId)
	if err != nil {
		s.logger.Error("Error while converting course id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	course, err := s.repo.GetCourseById(ctx, courseObjId)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			s.logger.Errorln("Course not found")
			return nil, status.Error(codes.InvalidArgument, "Course not found")
		}
		s.logger.Error("Error while getting course info ", err)
		return nil, grpcErrors.ErrInternal
	}

	lesson := models.Lesson{
		ID:          primitive.NewObjectID(),
		Name:        req.Name,
		Description: req.Description,
		Video:       models.Video{},
		CreatedAt:   time.Now().UTC(),
		UpdatedAt:   time.Now().UTC(),
	}

	err = s.repo.AddLessonToCourse(ctx, course.ID, lesson)
	if err != nil {
		s.logger.Error("Error while adding lesson to course ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.IncrementLessonCount(ctx, course.ID)
	if err != nil {
		s.logger.Error("Error while incrementing lesson count ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.AddLessonToCourseResponse{
		Lesson: &pb.Lesson{
			Id:          string(lesson.ID.Hex()),
			Name:        lesson.Name,
			Description: lesson.Description,
			Video:       nil,
			CreatedAt:   lesson.CreatedAt.String(),
			UpdatedAt:   lesson.UpdatedAt.String(),
		},
	}, nil
}

func (s *CourseService) AddVideoToLesson(ctx context.Context, req *pb.AddVideoToLessonRequest) (*pb.AddVideoToLessonResponse, error) {
	courseId, err := primitive.ObjectIDFromHex(req.CourseId)
	if err != nil {
		s.logger.Error("Error while converting course id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	course, err := s.repo.GetCourseById(ctx, courseId)
	if err != nil {
		s.logger.Error("Error while getting course info ", err)
		return nil, grpcErrors.ErrInternal
	}

	var lesson models.Lesson
	for _, l := range course.Lessons {
		if l.ID.Hex() == req.LessonId {
			lesson = l
			break
		}
	}

	if lesson.ID.IsZero() {
		return nil, status.Error(codes.InvalidArgument, "Lesson not found")
	}

	if !lesson.Video.ID.IsZero() {
		s.logger.Errorln("Video already present")
		return nil, status.Error(codes.InvalidArgument, "Video already present")
	}

	newVideoId := primitive.NewObjectID()
	newVideoKey := courseId.Hex() + "/" + lesson.ID.Hex() + "/" + newVideoId.Hex()

	video := models.Video{
		ID:        newVideoId,
		CreatedAt: time.Now().UTC(),
		UpdatedAt: time.Now().UTC(),
		Status:    models.VideoStatusOnGoing,
	}

	uploadRes, err := s.muxClient.UploadAsset(newVideoKey)
	if err != nil {
		s.logger.Error("Error while getting presigned upload url ", err)
		return nil, grpcErrors.ErrInternal
	}

	err = s.repo.AddVideoToLesson(ctx, lesson.ID, video)
	if err != nil {
		s.logger.Error("Error while adding video to lesson ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.AddVideoToLessonResponse{
		VideoId:            string(video.ID.Hex()),
		PreSignedUploadUrl: uploadRes.Data.Url,
	}, nil
}

func (s *CourseService) GetVideoInfo(ctx context.Context, req *pb.GetVideoInfoRequest) (*pb.GetVideoInfoResponse, error) {
	courseId, err := primitive.ObjectIDFromHex(req.CourseId)
	if err != nil {
		s.logger.Error("Error while converting course id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	course, err := s.repo.GetCourseById(ctx, courseId)
	if err != nil {
		s.logger.Error("Error while getting course info ", err)
		return nil, grpcErrors.ErrInternal
	}

	lessonId, err := primitive.ObjectIDFromHex(req.LessonId)
	if err != nil {
		s.logger.Error("Error while converting lesson id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	var lesson models.Lesson
	for _, l := range course.Lessons {
		if l.ID.Hex() == lessonId.Hex() {
			lesson = l
			break
		}
	}

	if lesson.ID.IsZero() {
		return nil, status.Error(codes.InvalidArgument, "Lesson not found")
	}

	if lesson.Video.ID.IsZero() {
		return nil, status.Error(codes.InvalidArgument, "Video not found")
	}

	if lesson.Video.Status != models.VideStatusReady {
		return nil, status.Error(codes.InvalidArgument, "Video not ready yet")
	}

	if !req.IsAdmin {
		if req.StudentId == 0 {
			return nil, status.Error(codes.InvalidArgument, "Student id not provided")
		}

		_, err := s.repo.GetStudentCourseMapping(ctx, req.StudentId, courseId)
		if err != nil {
			if err == mongo.ErrNoDocuments {
				return nil, status.Error(codes.PermissionDenied, "Student not enrolled in the course")
			}
			s.logger.Error("Error while getting student course mapping ", err)
			return nil, grpcErrors.ErrInternal
		}
	}

	token, err := s.muxClient.GetSignedToken(lesson.Video.PlaybackId)
	if err != nil {
		s.logger.Error("Error while getting presigned download url ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.GetVideoInfoResponse{
		Token:             token,
		PlaybackId:        lesson.Video.PlaybackId,
		CourseTitle:       course.Title,
		LessonTitle:       lesson.Name,
		LessonDescription: lesson.Description,
	}, nil
}

func (s *CourseService) PublishCourse(ctx context.Context, req *pb.PublishCourseRequest) (*pb.Empty, error) {
	courseId, err := primitive.ObjectIDFromHex(req.CourseId)
	if err != nil {
		s.logger.Error("Error while converting course id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	course, err := s.repo.GetCourseById(ctx, courseId)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, status.Error(codes.InvalidArgument, "Course not found")
		}
		s.logger.Error("Error while getting course info ", err)
		return nil, grpcErrors.ErrInternal
	}

	if course.CourseStatus == models.CourseStatusLive {
		return nil, status.Error(codes.InvalidArgument, "Course already published")
	}

	if len(course.Lessons) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Course does not have any lessons. Please add lessons to the course before publishing it")
	}

	for _, l := range course.Lessons {
		if l.Video.ID.IsZero() {
			return nil, status.Errorf(codes.InvalidArgument, "Lesson %s does not have a video. Please add videos to all lessons before publishing the course", l.Name)
		}

		if l.Video.Status != models.VideStatusReady {
			return nil, status.Errorf(codes.InvalidArgument, "Video for lesson %s is not ready yet. Please wait for the video to be ready before publishing the course", l.Name)
		}
	}

	err = s.repo.ChangeCourseStatusToPublished(ctx, course.ID)
	if err != nil {
		s.logger.Error("Error while changing course status to published ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.Empty{}, nil
}

func (s *CourseService) CheckIfStudentEnrolled(ctx context.Context, req *pb.CheckIfStudentEnrolledRequest) (*pb.CheckIfStudentEnrolledResponse, error) {
	courseId, err := primitive.ObjectIDFromHex(req.CourseId)
	if err != nil {
		s.logger.Error("Error while converting course id to object id ", err)
		return nil, grpcErrors.ErrInternal
	}

	mapping, err := s.repo.GetStudentCourseMapping(ctx, req.StudentId, courseId)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return &pb.CheckIfStudentEnrolledResponse{
				Enrolled: false,
			}, nil
		}
		s.logger.Error("Error while getting student course mapping ", err)
		return nil, grpcErrors.ErrInternal
	}

	return &pb.CheckIfStudentEnrolledResponse{
		Enrolled:   true,
		EnrolledOn: mapping.EnrolledOn.String(),
	}, nil
}
