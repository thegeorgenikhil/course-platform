package service

import (
	"context"
	"encoding/json"
	"strings"

	"github.com/thegeorgenikhil/course-platform/internal/course/mongo/models"
	grpcErrors "github.com/thegeorgenikhil/course-platform/pkg/grpc_errors"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type MuxWebhookPayload struct {
	EventType string    `json:"type"`
	EventData EventData `json:"data"`
}

type EventData struct {
	Passthrough string       `json:"passthrough"`
	PlaybackIds []PlaybackId `json:"playback_ids"`
	Tracks      []Track      `json:"tracks"`
}

type PlaybackId struct {
	Policy string `json:"policy"`
	Id     string `json:"id"`
}

type Track struct {
	Type     string  `json:"type"`
	Id       string  `json:"id"`
	Duration float32 `json:"duration"`
}

func (s *CourseService) MuxWebhook(ctx context.Context, req *pb.MuxWebhookRequest) (*pb.Empty, error) {
	var payload MuxWebhookPayload
	err := json.Unmarshal(req.Payload, &payload)
	if err != nil {
		s.logger.Errorln("error unmarshalling mux webhook payload ", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid payload")
	}

	switch payload.EventType {
	case "video.asset.ready":
		videoInfo := strings.Split(payload.EventData.Passthrough, "/")
		courseId, _ := primitive.ObjectIDFromHex(videoInfo[0])
		lessonId, _ := primitive.ObjectIDFromHex(videoInfo[1])
		videoId := videoInfo[2]

		course, err := s.repo.GetCourseById(ctx, courseId)
		if err != nil {
			s.logger.Error("Error while getting course info ", err)
			return nil, grpcErrors.ErrInternal
		}

		var lesson models.Lesson
		for _, l := range course.Lessons {
			if l.ID.Hex() == lessonId.Hex() {
				lesson = l
				break
			}
		}

		if lesson.ID.IsZero() {
			return nil, status.Error(codes.InvalidArgument, "Lesson not found")
		}

		if lesson.Video.ID.IsZero() {
			return nil, status.Error(codes.InvalidArgument, "Video not found")
		}

		video := lesson.Video
		if video.ID.Hex() != videoId {
			s.logger.Errorln("video id mismatch ", video.ID.Hex(), videoId)
			return nil, status.Error(codes.InvalidArgument, "Invalid video id")
		}

		video.Status = models.VideStatusReady
		video.PlaybackId = payload.EventData.PlaybackIds[0].Id
		video.Duration = payload.EventData.Tracks[0].Duration

		err = s.repo.UpdateVideo(ctx, courseId, lessonId, video)
		if err != nil {
			s.logger.Errorln("error updating video ", err)
			return nil, grpcErrors.ErrInternal
		}

		err = s.repo.UpdateLessonDuration(ctx, courseId, lessonId, video.Duration)
		if err != nil {
			s.logger.Errorln("error updating course info ", err)
			return nil, grpcErrors.ErrInternal
		}

		err = s.repo.UpdateCourseDuration(ctx, courseId, video.Duration)
		if err != nil {
			s.logger.Errorln("error updating course info ", err)
			return nil, grpcErrors.ErrInternal
		}

		return &pb.Empty{}, nil

	default:
		s.logger.Errorln("unknown mux webhook event type ", payload.EventType)
	}

	return &pb.Empty{}, nil
}
