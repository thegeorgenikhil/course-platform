package service

import (
	"context"
	"os"
	"strconv"
	"time"

	"github.com/stripe/stripe-go/webhook"
	"github.com/thegeorgenikhil/course-platform/internal/course/mongo/models"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *CourseService) StripeWebhook(ctx context.Context, req *pb.StripeWebhookRequest) (*pb.Empty, error) {
	webhookSecret := os.Getenv("STRIPE_WEBHOOK_SECRET")
	if webhookSecret == "" {
		s.logger.Errorln("STRIPE_WEBHOOK_SECRET not set")
		return nil, status.Error(codes.Internal, "STRIPE_WEBHOOK_SECRET not set")
	}

	event, err := webhook.ConstructEvent(req.Payload, req.StripeSignature, webhookSecret)
	if err != nil {
		s.logger.Errorln("error verifying webhook signature ", err)
		return nil, status.Error(codes.InvalidArgument, "Invalid payload")
	}

	switch event.Type {
	case "checkout.session.completed":
		session := event.Data.Object

		metadata, ok := session["metadata"].(map[string]interface{})
		if !ok {
			return nil, status.Error(codes.InvalidArgument, "Invalid metadata")
		}

		courseID, courseIDExists := metadata["course_id"].(string)
		studentID, studentIDExists := metadata["student_id"].(string)

		if !courseIDExists || !studentIDExists {
			return nil, status.Error(codes.InvalidArgument, "Invalid metadata")
		}

		courseObjId, err := primitive.ObjectIDFromHex(courseID)
		if err != nil {
			s.logger.Errorln("error converting course id to object id ", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid course id")
		}

		studentIdUint64, err := strconv.Atoi(studentID)
		if err != nil {
			s.logger.Errorln("error converting student id to uint64 ", err)
			return nil, status.Error(codes.InvalidArgument, "Invalid student id")
		}

		courseStudentMapping := models.CourseStudentMapping{
			CourseID:   courseObjId,
			StudentID:  uint64(studentIdUint64),
			EnrolledOn: time.Now(),
		}

		err = s.repo.CreateNewStudentCourseMapping(ctx, courseStudentMapping)
		if err != nil {
			s.logger.Errorln("error creating new student course mapping ", err)
			return nil, status.Error(codes.Internal, "Error creating new student course mapping")
		}

		checkoutSessionId, _ := session["id"].(string)
		amountTotalInPaisa := session["amount_total"].(float64)
		amountInRs := amountTotalInPaisa / 100
		currency, _ := session["currency"].(string)

		customerDetails, _ := session["customer_details"].(map[string]interface{})

		customerEmail, _ := customerDetails["email"].(string)
		customerName, _ := customerDetails["name"].(string)

		paymentObj := models.Payment{
			EventId:           event.ID,
			CheckoutSessionID: checkoutSessionId,
			CustomerEmail:     customerEmail,
			CustomerName:      customerName,
			Amount:            amountInRs,
			EventCreatedAt:    event.Created,
			Currency:          currency,
			Metadata:          metadata,
			CreatedAt:         time.Now(),
		}

		err = s.repo.CreatePayment(ctx, paymentObj)
		if err != nil {
			s.logger.Errorln("error creating payment ", err)
			return nil, status.Error(codes.Internal, "Error creating payment")
		}

		err = s.repo.IncrementCourseStudentCount(ctx, courseObjId)
		if err != nil {
			s.logger.Errorln("error incrementing course student count ", err)
			return nil, status.Error(codes.Internal, "Error incrementing course student count")
		}

		s.logger.Info("payment successful✅")
	default:
		s.logger.Info("unhandled event type: ", event.Type)
	}

	return &pb.Empty{}, nil
}
