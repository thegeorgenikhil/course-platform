package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Payment struct {
	ID                primitive.ObjectID `bson:"_id,omitempty"`
	EventId           string             `bson:"event_id"`
	CheckoutSessionID string             `bson:"checkout_session_id"`
	CustomerEmail     string             `bson:"email"`
	CustomerName      string             `bson:"name"`
	Amount            float64            `bson:"amount"`
	EventCreatedAt    int64              `bson:"event_created_at"`
	Currency          string             `bson:"currency"`
	Metadata          map[string]any     `bson:"metadata"`
	CreatedAt         time.Time          `bson:"created_at"`
}
