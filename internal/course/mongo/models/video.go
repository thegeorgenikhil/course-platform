package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Video struct {
	ID         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	PlaybackId string             `json:"playbackId" bson:"playbackId"`
	Status     VideoStatus        `json:"status" bson:"status"`
	Duration   float32            `json:"duration" bson:"duration"`
	CreatedAt  time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt  time.Time          `json:"updatedAt" bson:"updatedAt"`
}

type VideoStatus string

const (
	VideStatusReady    VideoStatus = "ready"
	VideoStatusOnGoing VideoStatus = "ongoing"
)
