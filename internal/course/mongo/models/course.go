package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Course struct {
	ID                    primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Title                 string             `json:"title" bson:"title"`
	Description           string             `json:"description" bson:"description"`
	Category              string             `json:"category" bson:"category"`
	CourseStatus          CourseStatus       `json:"courseStatus" bson:"courseStatus"`
	TotalStudentsEnrolled uint64             `json:"totalStudentsEnrolled" bson:"totalStudentsEnrolled"`
	TotalLessons          uint64             `json:"totalLessons" bson:"totalLessons"`
	Duration              float32            `json:"duration" bson:"duration"`
	Price                 uint64             `json:"price" bson:"price"`
	Lessons               []Lesson           `json:"lessons" bson:"lessons"`
	CreatedAt             time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt             time.Time          `json:"updatedAt" bson:"updatedAt"`
}

type CourseStatus string

const (
	CourseStatusDraft CourseStatus = "draft"
	CourseStatusLive  CourseStatus = "live"
)
