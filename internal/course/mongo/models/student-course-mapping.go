package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CourseStudentMapping struct {
	ID         primitive.ObjectID `bson:"_id,omitempty"`
	CourseID   primitive.ObjectID `bson:"course_id"`
	StudentID  uint64             `bson:"student_id"`
	EnrolledOn time.Time          `bson:"enrolled_on"`
}
