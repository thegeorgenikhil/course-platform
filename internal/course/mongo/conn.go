package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	ErrNoDocuments = mongo.ErrNoDocuments
)

type Mongo struct {
	db *mongo.Database
}

func NewMongo(ctx context.Context, connUri string, dbName string) (*Mongo, error) {
	conn := options.Client().ApplyURI(connUri)
	cl, err := mongo.Connect(ctx, conn)
	if err != nil {
		return nil, err
	}

	err = cl.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	db := cl.Database(dbName)

	return &Mongo{
		db: db,
	}, nil
}

func (m *Mongo) GetDB() *mongo.Database {
	return m.db
}

func (m *Mongo) Close(ctx context.Context) error {
	return m.db.Client().Disconnect(ctx)
}
