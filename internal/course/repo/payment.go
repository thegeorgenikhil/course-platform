package repo

import (
	"context"

	"github.com/thegeorgenikhil/course-platform/internal/course/mongo/models"
)

func (r *CourseRepo) CreatePayment(ctx context.Context, payment models.Payment) error {
	_, err := r.db.Collection("payments").InsertOne(ctx, payment)
	if err != nil {
		return err
	}

	return nil
}
