package repo

import (
	"context"

	"github.com/thegeorgenikhil/course-platform/internal/course/mongo/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (r *CourseRepo) CreateNewStudentCourseMapping(ctx context.Context, mapping models.CourseStudentMapping) error {
	_, err := r.db.Collection("student_course_mappings").InsertOne(ctx, mapping)
	if err != nil {
		return err
	}

	return nil
}

func (r *CourseRepo) GetEnrolledCourses(ctx context.Context, studentId uint64) ([]*models.Course, error) {
	var mappings []*models.CourseStudentMapping
	filter := bson.M{"student_id": studentId}
	cursor, err := r.db.Collection("student_course_mappings").Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	if err := cursor.All(ctx, &mappings); err != nil {
		return nil, err
	}

	var courses []*models.Course
	for _, mapping := range mappings {
		course, err := r.GetCourseById(ctx, mapping.CourseID)
		if err != nil {
			return nil, err
		}
		courses = append(courses, course)
	}

	return courses, nil
}

func (r *CourseRepo) GetStudentCourseMapping(ctx context.Context, studentId uint64, courseId primitive.ObjectID) (*models.CourseStudentMapping, error) {
	var mapping models.CourseStudentMapping
	filter := bson.M{"student_id": studentId, "course_id": courseId}
	err := r.db.Collection("student_course_mappings").FindOne(ctx, filter).Decode(&mapping)
	if err != nil {
		return nil, err
	}

	return &mapping, nil
}
