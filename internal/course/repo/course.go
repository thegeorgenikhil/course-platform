package repo

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/thegeorgenikhil/course-platform/internal/course/mongo/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type CourseRepo struct {
	logger *logrus.Logger
	db     *mongo.Database
}

func NewCourseRepo(logger *logrus.Logger, db *mongo.Database) *CourseRepo {
	return &CourseRepo{
		logger: logger,
		db:     db,
	}
}

func (r *CourseRepo) CreateNewCourse(ctx context.Context, course models.Course) (primitive.ObjectID, error) {
	insertResult, err := r.db.Collection("courses").InsertOne(ctx, course)
	if err != nil {
		return primitive.NilObjectID, err
	}

	return insertResult.InsertedID.(primitive.ObjectID), nil
}

func (r *CourseRepo) GetCourseById(ctx context.Context, courseId primitive.ObjectID) (*models.Course, error) {
	var course models.Course
	err := r.db.Collection("courses").FindOne(ctx, primitive.M{"_id": courseId}).Decode(&course)
	if err != nil {
		return nil, err
	}

	return &course, nil
}

func (r *CourseRepo) GetAllCourses(ctx context.Context) ([]*models.Course, error) {
	var courses []*models.Course
	cursor, err := r.db.Collection("courses").Find(ctx, primitive.M{})
	if err != nil {
		return nil, err
	}

	if err := cursor.All(ctx, &courses); err != nil {
		return nil, err
	}

	return courses, nil
}

func (r *CourseRepo) UpdateCourse(ctx context.Context, courseId primitive.ObjectID, title string, description string, category string, price uint64) error {
	filter := bson.M{"_id": courseId}
	update := bson.M{
		"$set": bson.M{
			"title":       title,
			"description": description,
			"category":    category,
			"price":       price,
			"updatedAt":   time.Now(),
		},
	}

	_, err := r.db.Collection("courses").UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (r *CourseRepo) UpdateCourseDuration(ctx context.Context, courseId primitive.ObjectID, duration float32) error {
	filter := bson.M{"_id": courseId}
	update := bson.M{
		"$inc": bson.M{
			"duration": duration,
		},
	}

	_, err := r.db.Collection("courses").UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (r *CourseRepo) IncrementCourseStudentCount(ctx context.Context, courseId primitive.ObjectID) error {
	filter := bson.M{"_id": courseId}
	update := bson.M{
		"$inc": bson.M{"totalStudentsEnrolled": 1},
	}

	_, err := r.db.Collection("courses").UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (r *CourseRepo) IncrementLessonCount(ctx context.Context, courseId primitive.ObjectID) error {
	filter := bson.M{"_id": courseId}
	update := bson.M{
		"$inc": bson.M{"totalLessons": 1},
	}

	_, err := r.db.Collection("courses").UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (r *CourseRepo) AddLessonToCourse(ctx context.Context, courseId primitive.ObjectID, lesson models.Lesson) error {
	filter := bson.M{"_id": courseId}
	update := bson.M{
		"$push": bson.M{"lessons": lesson},
		"$set":  bson.M{"updatedAt": time.Now()},
	}

	_, err := r.db.Collection("courses").UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (r *CourseRepo) UpdateLessonDuration(ctx context.Context, courseId primitive.ObjectID, lessonId primitive.ObjectID, duration float32) error {
	filter := bson.M{"_id": courseId, "lessons._id": lessonId}
	update := bson.M{
		"$inc": bson.M{"lessons.$.duration": duration},
	}

	_, err := r.db.Collection("courses").UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (r *CourseRepo) AddVideoToLesson(ctx context.Context, lessonId primitive.ObjectID, video models.Video) error {
	filter := bson.M{"lessons._id": lessonId}
	update := bson.M{
		"$set": bson.M{"lessons.$.video": video},
	}

	_, err := r.db.Collection("courses").UpdateOne(ctx, filter, update)

	if err != nil {
		return err
	}

	return nil
}

func (c *CourseRepo) UpdateVideo(ctx context.Context, courseId primitive.ObjectID, lessonId primitive.ObjectID, video models.Video) error {
	filter := bson.M{"_id": courseId, "lessons._id": lessonId}
	update := bson.M{
		"$set": bson.M{"lessons.$.video": video},
	}

	_, err := c.db.Collection("courses").UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (r *CourseRepo) ChangeCourseStatusToPublished(ctx context.Context, courseId primitive.ObjectID) error {
	filter := bson.M{"_id": courseId}
	update := bson.M{
		"$set": bson.M{"courseStatus": models.CourseStatusLive},
	}

	_, err := r.db.Collection("courses").UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}
