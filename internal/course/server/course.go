package server

import (
	"context"

	"github.com/thegeorgenikhil/course-platform/internal/course/service"
	pb "github.com/thegeorgenikhil/course-platform/proto/course-gen"
)

type Server struct {
	service *service.CourseService
}

// The following line is for better development experience, so that all the unimplemented methods and their params can be seen in the same file (by hovering) instead of having to go to the app.go file.
var server *Server = nil
var _ pb.CourseServiceServer = server

func NewCourseServer(courseService *service.CourseService) *Server {
	return &Server{
		service: courseService,
	}
}

func (s *Server) CreateCourse(ctx context.Context, req *pb.CreateCourseRequest) (*pb.CreateCourseResponse, error) {
	return s.service.CreateCourse(ctx, req)
}

func (s *Server) GetAllCourses(ctx context.Context, req *pb.Empty) (*pb.GetAllCoursesResponse, error) {
	return s.service.GetAllCourses(ctx, req)
}

func (s *Server) GetCourseById(ctx context.Context, req *pb.GetCourseByIdRequest) (*pb.GetCourseByIdResponse, error) {
	return s.service.GetCourseById(ctx, req)
}

func (s *Server) UpdateCourse(ctx context.Context, req *pb.UpdateCourseRequest) (*pb.Empty, error) {
	return s.service.UpdateCourse(ctx, req)
}

func (s *Server) AddLessonToCourse(ctx context.Context, req *pb.AddLessonToCourseRequest) (*pb.AddLessonToCourseResponse, error) {
	return s.service.AddLessonToCourse(ctx, req)
}

func (s *Server) AddVideoToLesson(ctx context.Context, req *pb.AddVideoToLessonRequest) (*pb.AddVideoToLessonResponse, error) {
	return s.service.AddVideoToLesson(ctx, req)
}

func (s *Server) GetVideoInfo(ctx context.Context, req *pb.GetVideoInfoRequest) (*pb.GetVideoInfoResponse, error) {
	return s.service.GetVideoInfo(ctx, req)
}

func (s *Server) PublishCourse(ctx context.Context, req *pb.PublishCourseRequest) (*pb.Empty, error) {
	return s.service.PublishCourse(ctx, req)
}

func (s *Server) BuyCourse(ctx context.Context, req *pb.BuyCourseRequest) (*pb.BuyCourseResponse, error) {
	return s.service.BuyCourse(ctx, req)
}

func (s *Server) GetEnrolledCourses(ctx context.Context, req *pb.GetEnrolledCoursesRequest) (*pb.GetEnrolledCoursesResponse, error) {
	return s.service.GetEnrolledCourses(ctx, req)
}

func (s *Server) CheckIfStudentEnrolled(ctx context.Context, req *pb.CheckIfStudentEnrolledRequest) (*pb.CheckIfStudentEnrolledResponse, error) {
	return s.service.CheckIfStudentEnrolled(ctx, req)
}

func (s *Server) StripeWebhook(ctx context.Context, req *pb.StripeWebhookRequest) (*pb.Empty, error) {
	return s.service.StripeWebhook(ctx, req)
}

func (s *Server) MuxWebhook(ctx context.Context, req *pb.MuxWebhookRequest) (*pb.Empty, error) {
	return s.service.MuxWebhook(ctx, req)
}
