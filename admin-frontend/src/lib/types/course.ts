export type Course = {
	id: string;
	title: string;
	description: string;
	category: string;
	course_status: string;
	total_lessons: number;
	lessons?: Lesson[];
	price: number;
	created_at: string;
};

export type Lesson = {
	id: string;
	name: string;
	description: string;
	preview_available: boolean;
	updated_at: string;
	video?: Video;
};

export type Video = {
	id: string;
	status: string;
	duration: number;
	created_at: string;
	updated_at: string;
};
