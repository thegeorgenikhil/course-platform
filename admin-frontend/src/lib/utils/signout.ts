export function signout(
	fetch: (input: RequestInfo | URL, init?: RequestInit | undefined) => Promise<Response>
) {
	fetch('/api/auth/logout');
}
