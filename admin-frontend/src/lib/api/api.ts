import { browser } from "$app/environment";
import { PUBLIC_API_URL } from "$env/static/public";

export const getAPIUrl = (path: string) => {
    if (import.meta.env.PROD  && !browser) {
        return `${PUBLIC_API_URL}${path}`;
    } else {
        return `http://localhost:8080${path}`;
    }
};
