import { getAPIUrl } from '$lib/api/api.js';
import { signout } from '$lib/utils/signout.js';
import { error, redirect } from '@sveltejs/kit';

export const load = async ({ locals, fetch }) => {
	const token = locals.token;
	const res = await fetch(getAPIUrl('/instructor/info'), {
		method: 'GET',
		headers: {
			authorization: `Bearer ${token}`
		}
	});

	const data = await res.json();
	if (res.status === 200) {
		return {
			info: {
				name: data.instructor.name,
				bio: data.instructor.bio || ''
			}
		};
	}

	if (res.status === 401) {
		signout(fetch);
	}

	return error(500, {
		message: data.message
	});
};

export const actions = {
	updateInstructor: async ({ fetch, request, locals }) => {
		const form = await request.formData();
		const name = form.get('name');
		const bio = form.get('bio');

		const res = await fetch(getAPIUrl('/instructor'), {
			method: 'PUT',
			body: JSON.stringify({ name, bio }),
			headers: {
				authorization: `Bearer ${locals.token}`
			}
		});

		const data = await res.json();

		if (res.status === 200) {
			redirect(302, '/admin/profile');
		}

		return error(500, {
			message: data.message
		});
	}
};
