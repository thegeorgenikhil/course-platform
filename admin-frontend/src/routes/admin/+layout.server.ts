import { redirect } from '@sveltejs/kit';

export const load = async ({ locals }) => {
	if (!locals.token) {
		redirect(302, '/login');
	}
};
