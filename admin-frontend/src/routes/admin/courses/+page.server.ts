import { getAPIUrl } from '$lib/api/api';
import type { Course } from '$lib/types/course.js';
import { signout } from '$lib/utils/signout';
import { error } from '@sveltejs/kit';

export async function load({ locals, fetch }) {
	const token = locals.token;
	const res = await fetch(getAPIUrl('/instructor/course'), {
		method: 'GET',
		headers: {
			authorization: `Bearer ${token}`
		}
	});

	const data = await res.json();
	if (res.status === 200) {
		return {
			courses: data.courses as Course[]
		};
	}

	if (res.status === 401) {
		signout(fetch);
	}

	return error(500, {
		message: data.message
	});
}
