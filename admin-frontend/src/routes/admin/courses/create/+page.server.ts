import { getAPIUrl } from '$lib/api/api.js';
import { signout } from '$lib/utils/signout.js';
import { error, redirect } from '@sveltejs/kit';

export const actions = {
	createCourse: async ({ locals, request, fetch }) => {
		const form = await request.formData();
		const title = form.get('title');
		const description = form.get('description');
		const category = form.get('category');
		const price = form.get('price');

		const token = locals.token;
		const res = await fetch(getAPIUrl('/instructor/course'), {
			method: 'POST',
			headers: {
				authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				title,
				description,
				category,
				price: Number(price)
			})
		});

		const data = await res.json();
		if (res.status === 200) {
			redirect(302, '/admin/courses');
		}

		if (res.status === 401) {
			signout(fetch);
		}

		return error(500, {
			message: data.message
		});
	}
};
