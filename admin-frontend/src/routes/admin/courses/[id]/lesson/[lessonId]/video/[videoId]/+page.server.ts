import { getAPIUrl } from '$lib/api/api.js';

export const load = async ({ locals, params, fetch }) => {
	const token = locals.token;
	const courseId = params.id;
	const lessonId = params.lessonId;
	const videoId = params.videoId;
	const res = await fetch(getAPIUrl(`/instructor/course/${courseId}/l/${lessonId}/v/${videoId}`), {
		method: 'POST',
		headers: {
			authorization: `Bearer ${token}`
		}
	});
	const data = await res.json();
	return {
		playbackId: data.playback_id,
		token: data.token,
		courseTitle: data.course_title,
		lessonTitle: data.lesson_title,
		lessonDescription: data.lesson_description
	};
};
