import { getAPIUrl } from '$lib/api/api';
import { signout } from '$lib/utils/signout.js';
import { error, redirect } from '@sveltejs/kit';

interface RouteParams {
	id: string;
}

export const load = async ({ locals, params, fetch }) => {
	const token = locals.token;
	const id = (params as RouteParams).id;
	const res = await fetch(getAPIUrl(`/instructor/course/${id}`), {
		method: 'GET',
		headers: {
			authorization: `Bearer ${token}`
		}
	});
	const data = await res.json();

	if (res.status === 401) {
		signout(fetch);
		return;
	}

	return {
		course: data.course
	};
};

export const actions = {
	addLesson: async ({ request, locals }) => {
		const token = locals.token;

		const form = await request.formData();
		const courseId = form.get('courseId');
		const lessonTitle = form.get('lessonTitle');
		const lessonDescription = form.get('lessonDescription');

		const res = await fetch(getAPIUrl('/instructor/course/l/new'), {
			method: 'POST',
			headers: {
				authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				course_id: courseId,
				name: lessonTitle,
				description: lessonDescription
			})
		});

		const data = await res.json();
		if (data.error) {
			return error(500, data.message);
		}

		if (res.status === 200) {
			redirect(302, `/admin/courses/${courseId}?tab=curriculum`);
		}
	},

	updateCourseDetails: async ({ request, locals, fetch }) => {
		const token = locals.token;
		const form = await request.formData();
		const courseId = form.get('courseId');
		const title = form.get('title');
		const description = form.get('description');
		const category = form.get('category');
		const price = form.get('price');

		const res = await fetch(getAPIUrl(`/instructor/course/${courseId}`), {
			method: 'PUT',
			headers: {
				authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				title,
				description,
				category,
				price: Number(price)
			})
		});
		const data = await res.json();
		if (res.status === 200) {
			redirect(302, `/admin/courses/${courseId}?tab=course-info`);
		}

		if (res.status === 401) {
			signout(fetch);
		}

		return {
			errors: {
				update: data.message
			},
			activeTab: 'course-info'
		};
	},
	publishCourse: async ({ request, locals }) => {
		const token = locals.token;
		const form = await request.formData();
		const courseId = form.get('courseId');

		const res = await fetch(getAPIUrl(`/instructor/course/publish`), {
			method: 'POST',
			headers: {
				authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				course_id: courseId
			})
		});
		const data = await res.json();

		if (res.status === 200) {
			redirect(302, `/admin/courses/${courseId}?tab=publish`);
		}

		return {
			errors: {
				course: data.message
			},
			activeTab: 'publish'
		};
	}
};
