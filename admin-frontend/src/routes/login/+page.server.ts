import { getAPIUrl } from '$lib/api/api';
import { redirect, type Actions } from '@sveltejs/kit';

export const load = async ({ locals }) => {
	if (locals.token) {
		redirect(302, '/admin/courses');
	}
};

export const actions: Actions = {
	login: async ({ fetch, request }) => {
		const form = await request.formData();
		const email = form.get('email');

		const res = await fetch(getAPIUrl('/instructor/login'), {
			method: 'POST',
			body: JSON.stringify({ email })
		});

		const data = await res.json();

		if (res.status === 200) {
			redirect(302, '/login/email-sent');
		}

		return {
			errors: {
				email: data.message
			}
		};
	}
};
