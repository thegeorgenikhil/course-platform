import { redirect } from '@sveltejs/kit';

export const actions = {
	goToAdminDashboard: () => {
		redirect(302, '/admin/courses');
	}
};
