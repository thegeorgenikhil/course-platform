import { getAPIUrl } from '$lib/api/api.js';
import { error, redirect } from '@sveltejs/kit';

export async function GET({ url, fetch, cookies }) {
	const verificationToken = url.searchParams.get('token');

	if (verificationToken === null) {
		throw error(400, 'No verification token provided');
	}

	const res = await fetch(getAPIUrl('/instructor/authenticate'), {
		method: 'POST',
		headers: {
			authorization: `Bearer ${verificationToken}`
		}
	});

	const data = await res.json();

	if (res.status === 200) {
		cookies.set('token', data.token, {
			// send cookie for every page
			path: '/',
			// server side only cookie so you can't use `document.cookie`
			httpOnly: true,
			// only requests from same site can send cookies
			// https://developer.mozilla.org/en-US/docs/Glossary/CSRF
			sameSite: 'strict',
			// only sent over HTTPS in production
			secure: false,
			// set cookie to expire after a month
			maxAge: 60 * 60 * 24 * 30
		});

		throw redirect(302, '/login/success');
	} else {
		return error(422, {
			message: data.message
		});
	}
}
