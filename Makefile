# CONSTANTS
docker_dev_microservices = docker-compose.dev.microservices.yml
docker_dev_services = docker-compose.dev.services.yml

# Why .PHONY https://stackoverflow.com/a/2145605

.PHONY: hello
hello:
	@echo "Hello, World" 

.PHONY: format
format:
	@echo "Formatting code..."
	go fmt ./...
	@echo "Done!"

.PHONY: path
path:
	export PATH="${PATH}:$(go env GOPATH)/bin"

# WEBHOOK COMMANDS (ONLY FOR LOCAL DEVELOPMENT) ===== 

# Stripe Listen command to listen for stripe webhooks when in local development
.PHONY: stripe_listen
stripe_listen:
	stripe listen --forward-to localhost:8080/course/stripe/webhook

# Ngrok command to expose localhost to the internet for testing Mux webhooks [Development ONLY]
# After running this command, copy the https url and create a new webhook in the Mux dashboard
# Example: https://19c1-152-58-166-219.ngrok-free.app
# The webhook endpoint is /course/mux/webhook
# The webhook url should be https://19c1-152-58-166-219.ngrok-free.app/course/mux/webhook
.PHONY: mux_ngrok
mux_ngrok:
	ngrok http http://localhost:8080

# ===================================================

# DEVELOPMENT COMMANDS ==============================

# dev_up_services_build: stops docker compose (if running), builds all projects and starts docker compose for the docker-compose.dev.services.yml file
.PHONY: dev_up_services_build
dev_up_services_build:
	@echo "Stopping docker images (if running...)"
	if [ "$(rm_vol)" = "true" ]; then \
		echo "Removing volumes..."; \
		docker compose -f $(docker_dev_services) down -v; \
	else \
		docker compose -f $(docker_dev_services) down; \
	fi
	@echo "Building (when required) and starting docker images..."
	docker compose -f $(docker_dev_services) up --build -d
	@echo "Docker images(services) built and started!"

# dev_down_services: stops docker compose (if running) for the docker-compose.dev.services.yml file
.PHONY: dev_down_services
dev_down_services:
	@echo "Stopping docker images (if running...)"
	docker compose -f $(docker_dev_services) down
	@echo "Docker images(services) stopped!"

# dev_up_microservices_build: stops docker compose (if running), builds all projects and starts docker compose for the docker-compose.dev.microservices.yml file
.PHONY: dev_up_microservices_build
dev_up_microservices_build:
	@echo "Stopping docker images (if running...)"
	docker compose -f $(docker_dev_microservices) down;
	@echo "Building (when required) and starting docker images..."
	docker compose -f $(docker_dev_microservices) up --build
	@echo "Docker images(microservice) built and started!"

# dev_down_microservices: stops docker compose (if running) for the docker-compose.dev.microservices.yml file
.PHONY: dev_down_microservices
dev_down_microservices:
	@echo "Stopping docker images (if running...)"
	docker compose -f $(docker_dev_microservices) down
	@echo "Docker images(microservice) stopped!"

# dev_admin_frontend_install: installs the admin frontend dependencies
.PHONY: dev_admin_frontend_install
dev_admin_frontend_install:
	@echo "Installing admin frontend dependencies..."
	cd admin-frontend && npm install
	@echo "Done!"

# dev_admin_frontend: starts the admin frontend
.PHONY: dev_admin_frontend
dev_admin_frontend:
	@echo "Starting admin frontend..."
	cd admin-frontend && NODE_ENV="development" npm run dev

# dev_frontend_install: installs the frontend dependencies
.PHONY: dev_frontend_install
dev_frontend_install:
	@echo "Installing frontend dependencies..."
	cd frontend && npm install
	@echo "Done!"

# dev_frontend: starts the frontend
.PHONY: dev_frontend
dev_frontend:
	@echo "Starting frontend..."
	cd frontend && NODE_ENV="development" npm run dev

# ===================================================

# PRODUCTION COMMANDS ===============================
## prod_up_build: stops docker compose (if running), builds all projects and starts docker compose
.PHONY: prod_up_build
prod_up_build:
	@echo "Stopping docker images (if running...)"
	docker compose down
	@echo "Building (when required) and starting docker images..."
	docker compose up --build -d
	@echo "Docker images built and started!"

## prod_down: stop docker compose
.PHONY: prod_down
prod_down:
	@echo "Stopping docker compose..."
	docker compose down
	@echo "Done!"

# ===================================================

# UTILITY COMMANDS===================================

# Proto Commands
## gen_proto: Generate protobuf files for a given proto file
## Usage Example: make gen_proto
## Reason for require_unimplemented_servers=false flag can be found here[IMPORTANT]:
## 1. https://stackoverflow.com/questions/65079032/grpc-with-mustembedunimplemented-method
## 2. https://github.com/grpc/grpc-go/issues/3669
.PHONY: gen_proto
gen_proto:
	@echo "Generating protobuf files..."
	protoc -I . \
	--go_out=. \
	--go_opt=module=github.com/thegeorgenikhil/course-platform \
	--go-grpc_out=require_unimplemented_servers=false:. \
	--go-grpc_opt=module=github.com/thegeorgenikhil/course-platform \
	proto/*.proto
	@echo "Done!"

# Migration File Commands
## gen_migrate: Generate .up .down migration files
## Usage Example: make gen_migrate dir=db/migrations/instructor name=instructor
.PHONY: gen_migrate
gen_migrate: 
	@migrate create -ext sql -dir $(dir) -seq $(name)

# ===================================================