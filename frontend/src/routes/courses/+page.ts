import { getAPIUrl } from '$lib/api/api';
import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch }) => {
	const res = await fetch(getAPIUrl('/student/courses'), {
		method: 'GET'
	});
	const data = await res.json();

	return {
		courses: data.courses
	};
};
