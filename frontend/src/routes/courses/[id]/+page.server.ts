import { getAPIUrl } from '$lib/api/api';
import { error } from '@sveltejs/kit';

interface RouteParams {
	id: string;
}

export const load = async ({ fetch, params, locals }) => {
	const token = locals.token
	const id = (params as RouteParams).id;
	const res = await fetch(getAPIUrl(`/student/courses/${id}`), {
		method: 'GET',
		headers: {
			authorization: token ? `Bearer ${token}` : ''
		}
	});
	const data = await res.json();
	
	if (res.status == 200) {
		return {
			course: data.course
		};
	}

	error(data.code, {
		message: data.message
	});
};