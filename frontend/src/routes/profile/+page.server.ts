import { getAPIUrl } from '$lib/api/api.js';
import { error, redirect } from '@sveltejs/kit';

export const load = async ({ locals, fetch }) => {
	const token = locals.token;
	const res = await fetch(getAPIUrl('/student/info'), {
		headers: {
			authorization: `Bearer ${token}`
		}
	});

	const data = await res.json();
	if (res.status === 200) {
		return {
			info: {
				name: data.student.name,
			}
		};
	}

	return error(500, {
		message: data.message
	});
};

export const actions = {
	updateStudent: async ({ fetch, request, locals }) => {
		const form = await request.formData();
		const name = form.get('name');

		const res = await fetch(getAPIUrl('/student'), {
			method: 'PUT',
			body: JSON.stringify({ name }),
			headers: {
				authorization: `Bearer ${locals.token}`
			}
		});

		const data = await res.json();

		if (res.status === 200) {
			redirect(302, '/profile');
		}

		return error(500, {
			message: data.message
		});
	}
};
