import { getAPIUrl } from '$lib/api/api.js';
import { error } from '@sveltejs/kit';

export const load = async ({ locals, params, fetch }) => {
    const token = locals.token;
    const courseId = params.courseId;
    const lessonId = params.lessonId;
    const videoId = params.videoId;

    // Use Promise.all to make the two API calls in parallel
    const [videoRes, courseRes] = await Promise.all([
        fetch(getAPIUrl(`/student/course/${courseId}/l/${lessonId}/v/${videoId}`), {
            method: "POST",
            headers: {
                authorization: `Bearer ${token}`
            }
        }),
        fetch(getAPIUrl(`/student/courses/${courseId}`), {
            method: 'GET',
            headers: {
                authorization: `Bearer ${token}`
            }
        })
    ]);

    const videoData = await videoRes.json();
    const courseData = await courseRes.json();

    if (videoRes.status === 200 && courseRes.status === 200) {
        return {
            video: {
                playbackId: videoData.playback_id,
                token: videoData.token,
                courseTitle: videoData.course_title,
                lessonTitle: videoData.lesson_title,
                lessonDescription: videoData.lesson_description,
            },
            course: {
                ...courseData.course,
            },
            currentLessonId: lessonId,
        };
    }

    // Handle errors for each API call
    if (videoData.code !== 200) {
        error(videoData.code, {
            message: videoData.message
        });
    }

    if (courseData.code !== 200) {
        error(courseData.code, {
            message: courseData.message
        });
    }
};
