import { redirect } from '@sveltejs/kit';

export async function GET({ cookies }) {
	cookies.set('token', '', {
		path: '/',
		expires: new Date(0)
	});

	// redirect the user
	throw redirect(302, '/auth/login');
}
