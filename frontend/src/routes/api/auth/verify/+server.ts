import { getAPIUrl } from '$lib/api/api.js';
import { error, redirect } from '@sveltejs/kit';

export async function GET({ url, fetch }) {
	const verificationToken = url.searchParams.get('token');

	if (verificationToken === null) {
		throw error(400, 'No verification token provided');
	}

	const res = await fetch(getAPIUrl('/student/verify-email'), {
		method: 'POST',
		body: JSON.stringify({ verification_code: verificationToken })
	});

	if (res.status === 200) {
		redirect(301, '/auth/verification-success');
	}

	const data = await res.json();
	throw error(data.code, data.message);
}
