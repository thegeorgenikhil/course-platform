import { redirect } from '@sveltejs/kit';

export const load = async ({ parent }) => {
	const token = (await parent()).token;

	if (token) {
		redirect(302, '/courses');
	}
};
