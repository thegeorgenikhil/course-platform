import { getAPIUrl } from '$lib/api/api';
import { redirect, type Actions } from '@sveltejs/kit';

export const actions: Actions = {
	login: async ({ fetch, request, cookies }) => {
		const form = await request.formData();
		const email = form.get('email');
		const password = form.get('password');

		const res = await fetch(getAPIUrl('/student/login'), {
			method: 'POST',
			body: JSON.stringify({ email, password })
		});

		const data = await res.json();

		if (res.status === 200) {
			cookies.set('token', data.token, {
				// send cookie for every page
				path: '/',
				// server side only cookie so you can't use `document.cookie`
				httpOnly: true,
				// only requests from same site can send cookies
				// https://developer.mozilla.org/en-US/docs/Glossary/CSRF
				sameSite: 'strict',
				// only sent over HTTPS in production
				secure: true,
				// set cookie to expire after a month
				maxAge: 60 * 60 * 24 * 30
			});

			redirect(302, '/courses');
		}

		if (data.message === 'Student is not verified') {
			redirect(302, `/auth/verify-email?email=${email}`);
		}

		return {
			errors: {
				message: data.message
			}
		};
	}
};
