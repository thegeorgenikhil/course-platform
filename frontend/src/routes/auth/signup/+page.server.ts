import { getAPIUrl } from '$lib/api/api';
import { redirect, type Actions } from '@sveltejs/kit';

export const actions: Actions = {
	signup: async ({ fetch, request }) => {
		const form = await request.formData();
		const name = form.get('name');
		const email = form.get('email');
		const password = form.get('password');

		const res = await fetch(getAPIUrl('/student/register'), {
			method: 'POST',
			body: JSON.stringify({ name, email, password })
		});

		if (res.status === 200) {
			redirect(301, `/auth/verify-email?email=${email}`);
		}
		
		const data = await res.json();
		return {
			errors: {
				message: data.message
			}
		};
	}
};
