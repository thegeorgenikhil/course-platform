import { getAPIUrl } from '$lib/api/api.js';
import { redirect } from '@sveltejs/kit';

export const load = async ({ url }) => {
	const email = url.searchParams.get('email');
	const resent = url.searchParams.get('resent') || false;
	if (!email) {
		redirect(302, '/auth/login');
	}

	return {
		email: email,
		resent: resent
	};
};

export const actions = {
	resendVerifcationEmail: async ({ fetch, request }) => {
		const form = await request.formData();
		const email = form.get('email');

		const res = await fetch(getAPIUrl('/student/resend-verification-mail'), {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		});

		if (res.status === 200) {
			redirect(301, `/auth/verify-email?email=${email}&resent=true`);
		}
	}
};
