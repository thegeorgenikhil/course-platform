import { getAPIUrl } from '$lib/api/api';
import { redirect, type Actions } from '@sveltejs/kit';

export const actions: Actions = {
	resetPassword: async ({ fetch, request }) => {
		const form = await request.formData();
		const password = form.get('password');
		const token = form.get('token');

		const res = await fetch(getAPIUrl('/student/reset-password'), {
			method: 'POST',
			body: JSON.stringify({ password, token })
		});

		if (res.status === 200) {
			redirect(301, `/auth/reset-success`);
		}
	}
};
