import { getAPIUrl } from '$lib/api/api';
import { redirect, type Actions } from '@sveltejs/kit';

export const actions: Actions = {
	forgotPassword: async ({ fetch, request }) => {
		const form = await request.formData();
		const email = form.get('email');
		const res = await fetch(getAPIUrl('/student/forgot-password'), {
			method: 'POST',
			body: JSON.stringify({ email })
		});

		const data = await res.json();
		if (res.status === 200) {
			redirect(302, '/auth/forgot-password/email-sent');
		}

		return {
			errors: {
				message: data.message
			}
		};
	}
};
