import { getAPIUrl } from '$lib/api/api';

export const load = async ({ fetch, locals, url }) => {
    const token = locals.token;
    const courseId = url.searchParams.get("course")
    const res = await fetch(getAPIUrl(`/student/courses/${courseId}`), {
        method: 'GET',
        headers: {
            authorization: token ? `Bearer ${token}` : ''
        }
    });
    const data = await res.json();

    return {
        course: data.course
    }
};