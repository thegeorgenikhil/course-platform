export const formatSeconds = (seconds: number) => {
    if (seconds >= 3600) {
        return `${Math.floor(seconds / 3600)}h ${Math.floor((seconds % 3600) / 60)}m`;
    }

    if (seconds >= 60) {
        return `${Math.ceil(seconds / 60).toFixed(0)}m`;
    }

    return `${seconds.toFixed(0)}s`;
}