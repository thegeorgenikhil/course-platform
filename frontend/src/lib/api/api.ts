import { PUBLIC_API_URL } from "$env/static/public";
import { browser } from "$app/environment";

export const getAPIUrl = (path: string) => {
    if (import.meta.env.PROD && !browser) {
        return `${PUBLIC_API_URL}${path}`;
    } else {
        return `http://localhost:8080${path}`;
    }
};
