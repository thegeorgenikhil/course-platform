package common

const (
	LoginInstructorEventName = "login_instructor_event"

	RegisterStudentEventName           = "register_student_event"
	ResendStudentVerificationEventName = "resend_student_verification_event"
	ResetStudentPasswordEventName      = "reset_student_password_event"

	LoginInstructorSubject = "Login to your account"

	RegisterEmailSubject      = "Verify your email address"
	ResetPasswordEmailSubject = "Reset your password"
)

type Event struct {
	Name string `json:"name"`
	Data any    `json:"data"`
}

type LoginInstructorEvent struct {
	Name string                      `json:"name"`
	Data LoginInstructorEventPayload `json:"data"`
}

type LoginInstructorEventPayload struct {
	Email     string `json:"email"`
	AuthToken string `json:"auth_token"`
	Subject   string `json:"subject"`
}

type RegisterEvent struct {
	Name string               `json:"name"`
	Data RegisterEventPayload `json:"data"`
}

type RegisterEventPayload struct {
	Name              string `json:"name"`
	Email             string `json:"email"`
	VerificationToken string `json:"verification_token"`
	Subject           string `json:"subject"`
}

type ResetPasswordEvent struct {
	Name string                    `json:"name"`
	Data ResetPasswordEventPayload `json:"data"`
}

type ResetPasswordEventPayload struct {
	Name               string `json:"name"`
	Email              string `json:"email"`
	ResetPasswordToken string `json:"reset_password_token"`
	Subject            string `json:"subject"`
}
