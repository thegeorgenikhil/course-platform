package common

const (
	RABBITMQ_MAILER_EXCHANGE = "mailer-exchange"
	RABBITMQ_MAILER_QUEUE    = "mailer-queue"

	RABBITMQ_INSTRUCTOR_MAIL_ROUTING_KEY = "instructor-mail"
	RABBITMQ_STUDENT_MAIL_ROUTING_KEY    = "student-mail"

	RABBITMQ_CONTENT_TYPE_JSON = "application/json"
	RABBITMQ_CONTENT_TYPE_TEXT = "text/plain"
)
