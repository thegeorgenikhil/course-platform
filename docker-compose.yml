version: '3'

services:
  admin-frontend:
    build:
      context: .
      dockerfile: ./admin-frontend/Dockerfile
    ports:
      - 3000:3000
    depends_on:
      - api-gateway    
    networks:
      - course-platform-network

  frontend:
    build:
      context: .
      dockerfile: ./frontend/Dockerfile
    ports:
      - 3001:3001
    depends_on:
      - api-gateway    
    networks:
      - course-platform-network
      
  api-gateway:
    build:
      context: .
      dockerfile: ./docker/Dockerfile-api_gateway
    ports:
      - 8080:8080
    networks:
      - course-platform-network
    depends_on:
      - instructor-service
      - student-service
      - course-service

  instructor-service:
    build:
      context: .
      dockerfile: ./docker/Dockerfile-instructor
    ports:
      - 5001:5001
    networks:
      - course-platform-network
    depends_on:
      postgres:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy

  student-service:
    build:
      context: .
      dockerfile: ./docker/Dockerfile-student
    ports:
      - 5002:5002
    networks:
      - course-platform-network
    depends_on:
      postgres:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy

  course-service:
    build:
      context: .
      dockerfile: ./docker/Dockerfile-course
    env_file:
      - .env.production
    environment:
      - STRIPE_REDIRECT_URL=http://localhost:3001
    ports:
      - 5003:5003
    networks:
      - course-platform-network
    depends_on:
      - instructor-service
      - student-service

  mailer-service:
    build:
      context: .
      dockerfile: ./docker/Dockerfile-mailer
    ports:
      - 5101:5101
    environment:
      - ADMIN_FRONTEND_URL=http://localhost:3000
      - FRONTEND_URL=http://localhost:3001
    networks:
      - course-platform-network
    depends_on:
      rabbitmq:
        condition: service_healthy

  rabbitmq:
    image: rabbitmq:3.11-management-alpine
    environment:
      RABBITMQ_DEFAULT_USER: guest
      RABBITMQ_DEFAULT_PASS: guest
    healthcheck:
      test: [ "CMD", "nc", "-z", "localhost", "5672" ]
      interval: 2s
      timeout: 2s
      retries: 10
    ports:
      - "5672:5672"
      - "15672:15672"
    networks:
      - course-platform-network

  mongodb:
    image: mongo
    ports:
      - "27017:27017"
    networks:
      - course-platform-network
    volumes:
      - mongo-data:/data/db

  postgres:
    image: postgres
    environment:
      - POSTGRES_DB=course_platform_db
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=password
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "pg_isready -d $${POSTGRES_DB} -U $${POSTGRES_USER}"
        ]
      interval: 3s
      timeout: 3s
      retries: 30
    ports:
      - "5432:5432"
    networks:
      - course-platform-network
    volumes:
      - postgres-data:/var/lib/postgresql/data

volumes:
  postgres-data:
  mongo-data:

networks:
  course-platform-network: